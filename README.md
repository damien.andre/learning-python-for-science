# learning-python-for-science

This document is a practical introduction to the Python language for scientist. 
After a quick introduction to the basis of Python, this document introduces
numpy, scipy, matplotlib, pandas and sympy modules. 

The second part is dedicated to an introduction to neural network. Here, for pedagogical 
reasons, it is proposed to start neural network 'from scratch' without any dedicated 
library such as keras or scikit learn. 

The document was created with latex and most of images were created with the inkscape software.
To build the document on a standard GNU/Linux computer simply do : 
```
$ cd latex
$ pdflatex ./poly.tex
```

Enjoy ! :)

