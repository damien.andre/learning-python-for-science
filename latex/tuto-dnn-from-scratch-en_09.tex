\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}
\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newdnnexo{09}{Pre-processing data}

\noindent Remember this simple sentence: \textbf{bad data = bad
  predictions}.

So, preparing data is really important for machine learning.  In real
life, this process is probably the most time-consuming step of machine
learning. You need to automatize the process of collecting data,
removing outlier points, removing non-useful data and so on... It can
be really complicated!

In this document, we deal only with already-prepared data available in
machine learning frameworks. By consequent, this preliminary work,
that consists in cleaning data, was already done for you. Indeed, you have
to keep in mind that this is a very important work that must be done
on real data set.

However, even if our data are clean, we can apply some numerical
treatments in order to optimize the learning process. The aim of this
numerical process is to scale input data in a range close to $[0, 1]$.
This step is called \textit{feature scaling}, it allows to facilitate
the learning step.

To achieve this goal, several methods exist. To get an overview, you
can visit this
\href{https://en.wikipedia.org/wiki/Feature\_scaling}{wikipedia
  page}. My favorite choice came from philosophical reason: in
science, it is considered that \textit{nature} often gives us data
that follows normal distributions. In such cases, a good
\textit{feature scaling} is the \textit{standardization} defined as:
\begin{equation}
  x \rightarrow \frac{x - \bar{x}}{\sigma}
\end{equation}
where $x$ is the original data, $\bar{x}$ the mean data value and
$\sigma$ the standard deviation. Thanks to numpy,
\textit{standardization} is really easy to obtain:
\vspace*{-1\baselineskip}
\begin{lstlisting}
x = (x - x.mean()) / x.std() 
\end{lstlisting}
where \keyw{x} is a numpy array that contains the data set.

A last trick is to initialize weights and bias to random values in
the range $[0,1]$. It allows to optimize the stochastic gradient
\textbf{for already scaled data}.

\section*{Let's code!}

\begin{manip}
  Thanks to the first tutorial, create a Python environment \texttt{(dnn)} and launch the \textbf{thonny} editor.
\end{manip}

\begin{manip}
  Download the
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/dnn/mltool.py}{\texttt{mltool.py}}
   file and put it in your current working dir.
\end{manip}

\begin{manip}
  Download the \dltutodnn{09}{tuto-09.py} script file and
  execute it with \textbf{thonny}. This file will be used as starting point for
  this tutorial. Note that this file is simply the correction of the previous tutorial.
\end{manip}

\begin{task}
  Edit this script file for implementing \textit{feature scaling} and
  randomization of weights and bias.
\end{task}

\begin{info}
  The correction of this part is given by \dltutodnn{09}{tuto-09\_correction-part1.py}
\end{info}

\section*{Conclusion and application to multi-classification}

As you can see, a single neural network is not very hard to
implement. Indeed, it uses a lot of tips and tricks :
\begin{itemize}
\item the feed forward and back propagation step;
\item differentiable activation functions such as sigmoid function;
\item loss functions;
\item gradient descent algorithm;
\item shuffling and slicing data in mini-batches;
\item data pre-processing with  \textit{feature scaling};
\item randomization of weights and bias.
\end{itemize}
All these ingredients enable the implementation of a complete
mono-neural network Perceptron. Now, we can encapsulate our
mono-neural Perceptron into à Python class as it is shown below:
\vspace*{-1.5\baselineskip}
\begin{lstlisting}
class Perceptron:
    def __init__(self, n):
        self.w = np.random.rand(n) 
        self.b = np.random.rand(1) 

    def train(self, x, t, eta, epoch, mb):
        # some useful variable
        self.loss = np.zeros(epoch)

        for i in range(epoch):
            s = np.arange(x.shape[0])
            np.random.shuffle(s)
            x_s = np.split(x[s], mb)
            t_s = np.split(t[s], mb)
            l = 0.
            for xb,tb in zip(x_s, t_s):
                m = len(t_s)

                # feed forward 
                a = np.dot(xb, self.w) + self.b
                z = sigmoid(a)
                y = z

                l += (1./m) * np.sum( (y-tb)**2 ) # record the current value of loss

                # backward propagation
                self.w -= (2./m)*eta * np.dot(((y-tb)*sigmoid_prime(a)).T, xb)
                self.b -= (2./m)*eta * np.sum( (y-tb)*sigmoid_prime(a))
            self.loss[i] = l

    def predict(self, x):
        a = np.dot(x, self.w) + self.b
        z = sigmoid(a)
        return z
\end{lstlisting}

\begin{manip}
  Download the
   \dltutodnn{09}{perceptron.py} script file that contains the Perceptron class and put this file into your current working dir.
\end{manip}

\begin{task}
  Based on the correction of the previous section \dltutodnn{09}{tuto-09\_correction-part1.py}, use the proposed
  Perceptron class for solving linear multi-classification
  problems with several instances of Perceptron (one for each
  class). For three classes problem you must obtain something like
  this:
\begin{center}
  \includegraphics[width=.6\linewidth]{multi-class-perceptron}
\end{center}
\end{task}

\begin{info}
  The correction of this part is given by \dltutodnn{09}{tuto-09\_correction-part1.py}
\end{info}


Congratulation! you mastering the mono-neural network! Now, we will move on the implementation of 
multi-layer neural networks to deal with non-linearly separable data classification.


\end{document}
