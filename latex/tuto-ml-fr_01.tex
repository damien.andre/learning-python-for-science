\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\activity{\textbf{Crédit} : \href{https://github.com/cjlux/APP-ML/blob/master/APP-ML\_2023/Notebooks}{JL. Charles} (CC BY-SA 4.0)}




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newmlexo{01}{configurer son environnement Python}

\noindent L'objectif de ce tuto est de vous montrer comment configurer
votre environnement Python pour faire du machine learning. En effet,
de nombreuses bibliothèques Python sont nécessaires et la difficulté
majeure est alors de gérer correctement toutes ces bibliothèques sans
collision de versions. Pour cela, nous allons utiliser l'outil
\textbf{conda} qui permet de cloisonner sur un même ordinateur
plusieurs environnements Python et d'éviter ainsi de nombreux
problèmes de dépendance et de version des bibliothèques.

\begin{info}
  Voici les différentes bibliothèques nécessaires pour le machine learning
  \begin{center}
  \begin{tabular}{l l l}
    \texttt{tensorflow} & -> & bibliothèque de machine learning (développé par Google) \\
    \texttt{keras}      & -> & l'interface de \texttt{tensorflow} pour le langage Python \\
    \texttt{numpy}      & -> & l'indispensable numpy pour les tableaux \\
    \texttt{matplotlib} & -> & pour réaliser divers tracés graphiques \\
    \texttt{opencv}     & -> & traitement d'image en temps réel (initialement développé par Intel) \\
    \texttt{thonny}     & -> & un éditeur de texte léger dédié au langage Python
  \end{tabular}
  \end{center}
\end{info}



\subsection*{Partie 1 : configuration de votre environnement Python}

\begin{manip}
  Créez dans votre répertoire "\texttt{P:}" un dossier "\texttt{info-s4}" qui sera votre répertoire de
  travail.
\end{manip}

\begin{manip}
  Ouvrez "\textit{anaconda power shell prompt}" dans le menu Windows
\end{manip}

\begin{manip}
  Dans le prompt, allez dans votre répertoire de travail avec la
  commande \texttt{cd}
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
(base) PS C:\Users\xxxxx> cd P:\info-s4\
(base) PS P:\info-s4> _
\end{lstlisting}
\end{manip}

\begin{manip}
  Créez un environnement virtuel conda qui se nommera "\texttt{ml}" : 
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
(base) PS P:\info-s4> conda create -n ml pip python=3.8
(base) PS P:\info-s4> _
\end{lstlisting}
\end{manip}

\begin{info}
Vous pouvez répondre toujours "oui" aux questions posées par \textit{conda}.
\end{info}


\begin{manip}
  Activez votre environnement virtuel conda \texttt{ml} : 
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
(base) PS P:\info-s4> conda activate ml
(ml) PS P:\info-s4> _
\end{lstlisting}
\end{manip}

\begin{info}
  À partir de maintenant, toutes les bibliothèques que vous allez
  installer via l'outil \textbf{pip} ne seront disponibles
  \textbf{que} dans cet environnement virtuel \texttt{(ml)}. 
\end{info}

\begin{task}
  Observez le prompt. Comment savoir quel environnement virtuel est actif ?  
\end{task}

\begin{manip}
  Téléchargez le fichier
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/ml/requirements.txt}{\texttt{requirements.txt}}
  et placez ce fichier dans votre répertoire de travail \keyw{\texttt{info-s4}}. 
\end{manip}

\begin{info}
  Le fichier \texttt{requirements.txt} est un simple fichier texte qui
  liste les librairies à télécharger et à installer. Ce fichier
  précise également les versions des librairies.
\end{info}

\begin{task}
  Ouvrez le fichier \texttt{requirements.txt} avec
  \textbf{notepad++} et comprenez sa structure.
\end{task}

\begin{manip}
  Maintenant, vous allez installer les bibliothèques du fichier
  \texttt{requirements.txt} à l'aide de \textbf{pip}. Pour cela,
  tapez la commande suivante dans le prompt:
    \vspace*{-1.5\baselineskip}
\begin{lstlisting}
(ml) PS P:\info-s4> pip install -r requirements.txt
(ml) PS P:\info-s4> _
\end{lstlisting}
    \begin{description}
    \item[Remarque 1] attention, vous devez vérifier que l'environnement \texttt{(ml)} est bien actif !
    \item[Remarque 2] cette opération prend généralement 5 minutes, soyez patient.
    \end{description}
\end{manip}

\begin{info}
  Félicitation, votre environnement Python \texttt{(ml)}
  est prêt pour le machine learning !
\end{info}

\subsection*{Partie 2 : vérification de votre environnement Python}
Vous allez maintenant vérifier que votre environnement Python
\texttt{(ml)} est fonctionnel. Vous allez utiliser l'éditeur de texte
\textbf{thonny} qui est facile, léger et dédié au langage Python.

\begin{manip}
  Ouvrez \textbf{thonny} en tapant dans le prompt:
  \vspace*{-1.5\baselineskip}
  \begin{lstlisting}
(ml) PS P:\info-s4> thonny
(ml) PS P:\info-s4> _
\end{lstlisting}
\end{manip}

\begin{manip}
  Dans \textbf{thonny}, exécutez le code python suivant:
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2
print("Python     : {}".format(sys.version.split()[0]))
print("tensorflow : {} incluant keras {}".format(tf.__version__, keras.__version__))
print("numpy      : {}".format(np.__version__))
print("cv2        : {}".format(cv2.__version__))
\end{lstlisting}
\end{manip}

\begin{info}
  Ce script est également disponible ici -> \dltutoml{01}{test.py}
\end{info}

\begin{task}
  Exécutez ce script et vérifiez que les librairies sont bien installées. Vérifiez leurs versions. 
\end{task}

\begin{info}
  Bravo, vous avez configuré un environnement Python fonctionnel pour le
  machine learning !
\end{info}

\subsection*{Partie 3 : résumé des étapes clés}

\begin{info}
Les étapes ci-dessus vous seront utiles à chaque fois que vous voudrez
faire du machine learning sur un nouveau PC. Aussi, voici un résumé
des commandes utiles.
\end{info}

\textit{Ouvrez "\textit{anaconda power shell prompt}", puis allez dans votre
répertoire de travail}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(base) PS C:\Users\xxxxxx> cd P:\info-s4
\end{lstlisting}
\vspace*{-0.5\baselineskip}
\textit{Créez l'environnement Python \texttt{(ml)} si celui-ci n'existe pas}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(base) PS P:\info-s4> conda create -n ml pip python=3.8 # si (ml) existe, repondez non
\end{lstlisting}
\vspace*{-0.5\baselineskip}
\textit{Activez l'environnement Python \texttt{(ml)}}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(base) PS P:\info-s4> conda activate ml
\end{lstlisting}
\vspace*{-0.5\baselineskip}
\textit{Téléchargez le fichier \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/ml/requirements.txt}{\texttt{requirements.txt}} avec le prompt}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(ml) PS P:\info-s4> Invoke-WebRequest -URI https://gitlab.com/damien.andre/learning-python-for-science/-/raw/master/script/ml/requirements.txt -OutFile ./requirements.txt
\end{lstlisting}
\vspace*{-0.5\baselineskip}
\textit{Installez les bibliothèques Python avec \textbf{pip}}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(ml) PS P:\info-s4> pip install -r requirements.txt
\end{lstlisting}
\vspace*{-0.5\baselineskip}
\textit{Exécutez thonny}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(ml) PS P:\info-s4> thonny
\end{lstlisting}

\begin{info}
Installer un environnement Python dédié au machine learning prend
beaucoup de place sur le disque dur. Il est donc conseillé de
nettoyer vos environnements Python lorsque vous avez fini votre
travail. Pour cela vous pouvez utiliser les commandes suivantes.
\end{info}


\textit{Si vous êtes déjà dans l'environnement Python \texttt{(ml)}, quittez-le}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(ml) PS P:\info-s4> conda deactivate -n ml
\end{lstlisting}
\vspace*{-0.5\baselineskip}
\textit{Puis, supprimez l'environnement Python \texttt{(ml)}}
\vspace*{-1.6\baselineskip}
    \begin{lstlisting}
(base) PS P:\info-s4> conda env remove -n ml
\end{lstlisting}

\end{document}
