\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Débuter avec numpy et matplotlib}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}
\lstset{language=granPython,style=granPython}

\newmplexo{02}{La balle rebondissante}

\noindent L'objectif est d'extraire, de post-traiter et de tracer les
données générées par une simulation. Vous pouvez regarder la
simulation ici \dl{bouncing-ball.mp4}, il s'agit simplement d'une
balle qui rebondit sur le sol.  Maintenant, téléchargez le fichier de
données \dl{bouncing-ball.txt} généré par cette simulation. Comme vous
pouvez le voir, le fichier de données contient trois colonnes :
\begin{enumerate}
\item le temps écoulé en secondes, 
\item la position de la balle selon $x$ (coordonnées horizontales) en mètres,
\item la position de la balle selon $y$ (coordonnées verticales) en mètres.
\end{enumerate}

Vous pouvez voir ici l'en-tête de ce fichier de données.
 \vspace*{-1\baselineskip}
\begin{lstlisting}
# time (s)     # positionX (m)       # positionY (m)
0.0            10.04                 79.99984304
0.16           11.639999999999965    79.86485743999998
0.32           13.239999999999931    79.47873583999998
0.48           14.839999999999897    78.84147824
...
\end{lstlisting}



\subsection*{Partie 1 : affichage de la trajectoire de la balle}

\begin{manip}
  Téléchargez le fichier de données \dl{bouncing-ball.txt} et mettez ce fichier dans votre répertoire de travail courant. 
\end{manip}

\begin{task}
  Extraire les données grâce à la fonction
  \href{https://numpy.org/doc/stable/reference/generated/numpy.loadtxt.html}{\texttt{loadtxt()}}
  du module \texttt{numpy}.  Stockez chaque colonne du fichier de
  données dans un tableau numpy. Pour cela, vous pouvez utiliser le
  code suivant.  \vspace*{-2\baselineskip}
\begin{lstlisting}
import numpy as np    
t, p_x, p_y = np.loadtxt("./bouncing-ball.txt", delimiter='\t', usecols=(0, 1, 2), unpack=True)
\end{lstlisting}
\end{task}

\begin{task}
  En vous inspirant du tuto précédant, tracez la trajectoire de la
  balle avec \texttt{matplotlib} de façon à obtenir la courbe
  suivante.
\end{task}

\begin{center}
  \includegraphics[width=0.4\linewidth]{img/bouncing-ball_2.pdf}
\end{center}

\begin{info}
  Comme vous pouvez le constater, les échelles verticales et horizontales sont différentes, ce qui déforme la trajectoire de la balle.
\end{info}

\begin{task}
  L'extrait de code suivant permet de contraindre matplotlib à utiliser les mêmes échelles verticales et horizontales.
   \vspace*{-1\baselineskip}
\begin{lstlisting}
plt.gca().set_aspect('equal', adjustable='box')
\end{lstlisting}
   Utilisez ce code pour ajuster les échelles et ne plus avoir un aspect déformé de la trajectoire.
 \end{task}

 \begin{task}
   Ajustez les limites du graphe en utilisant les fonctions
   \href{https://www.geeksforgeeks.org/matplotlib-pyplot-xlim-in-python/}{\texttt{xlim()}}
   et
   \href{https://www.geeksforgeeks.org/matplotlib-pyplot-xlim-in-python/}{\texttt{ylim()}} de \texttt{matplotlib}
   de façon à obtenir le graphe suivant.
\end{task}

\begin{center}
  \includegraphics[width=0.9\linewidth]{img/bouncing-ball_3.pdf}
\end{center}


\begin{info}
  La correction de cette partie est donnée par
  \dltutompl{02}{part-1.py}
\end{info}

\subsection*{Partie 2 : un peu de calcul}

Les paramètres physiques utilisés dans la simulation sont les suivants :

\begin{tabular}{ll|ll}
  gravité           & & $\vec{g}=\begin{bmatrix}0 \\ -9.81\end{bmatrix}$ & $\left( \text{m}.\text{s}^{-2} \right)$\\
  position initiale & (à $t_0 = 0\ \text{s}$) & $\vec{p}(t_0)=\begin{bmatrix}10 \\ 80\end{bmatrix}$ & $\left( \text{m} \right)$ \\
  vitesse initiale  & (à $t_0 = 0\ \text{s}$) & $\vec{v}(t_0)=\begin{bmatrix}10 \\ 0\end{bmatrix}$ & $\left( \text{m}.\text{s}^{-1} \right)$
\end{tabular}


\begin{info}
  En appliquant la 3ème loi de Newton pour une masse ponctuelle avec
  les paramètres ci-dessus, il est possible d'en déduire les équations
  de mouvement de la balle. Ces équations décrivent la position de la balle $\vec{p}(t)$ en
  fonction du temps $t$:
  \begin{equation}
    \vec{p}(t) = \begin{bmatrix}10 t + 10 \\ -\frac{9.81}{2} t^{2} + 80\end{bmatrix}\label{eq:a}
  \end{equation}
\end{info}

\begin{info}
  Nous avons donc deux équations de mouvement : une équation selon $x$ et une équation selon $y$.
  \begin{equation}
    p_x(t) = 10 t + 10 \label{eq:x}
  \end{equation}
  \begin{equation}
    p_y(t) = -\frac{9.81}{2} t^{2} + 80 \label{eq:y}
  \end{equation}
  
\end{info}

\begin{manip}
  Vérifiez mathématiquement que les conditions initiales
  $\vec{p}(t=0)$ et $\vec{v}(t=0)$ sont bien respectées.
\end{manip}


L'extrait de code suivant permet d'appliquer l'équation \ref{eq:x} qui
décrit l'évolution théorique \texttt{p\_xth} de la position de la
balle selon l'axe $x$: \vspace*{-1\baselineskip}
\begin{lstlisting}
# read data file 
t, p_x, p_y = np.loadtxt("./bouncing-ball.txt", delimiter='\t', usecols=(0, 1, 2), unpack=True)
# equation of motion
p_xth = 10. * t + 10.
\end{lstlisting}


\begin{task}
  En utilisant cet extrait de code et l'équation \ref{eq:y}, calculer
  également l'évolution \texttt{p\_yth} de la position théorique de la
  balle selon l'axe $y$.
\end{task}

\begin{task}
  Tracez le résultat de façon à comparer la simulation et la position
  théorique que vous venez de calculer avec \texttt{p\_xth} et
  \texttt{p\_yth}. Ajustez les limites du graphe de façon à ne pas
  afficher les rebonds ($x \in [0,50])$. Vous devriez obtenir la courbe suivante.
\end{task}

 \begin{center}
  \includegraphics[width=0.3\linewidth]{img/bouncing-ball_5.pdf}
\end{center}

\begin{info}
  La correction de cette partie est donnée par
  \dltutompl{02}{part-2.py}
\end{info}

\subsection*{Partie 3 : pour aller plus loin, tracé des vecteurs vitesses}

\begin{info}
Reprenez le script de la partie 1.
\end{info}

\begin{task}
  Grâce à la fonction
  \href{https://numpy.org/doc/stable/reference/generated/numpy.gradient.html}{\texttt{gradient()}}
  de \texttt{numpy}, calculez les composantes \texttt{v\_x} et
  \texttt{v\_y} du vecteur vitesse à partir des positions \texttt{p\_x} et
  \texttt{p\_y}.
\end{task}

\begin{task}
  Grâce à la fonction
  \href{https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.arrow.html}{\texttt{arrow()}}
  de \texttt{matplotlib}, tracez ces vecteurs vitesses de façon à obtenir l'image suivante.
\end{task}

  \begin{center}
  \includegraphics[width=0.9\linewidth]{img/bouncing-ball_4.pdf}
\end{center}

\begin{info}
  La correction de cette partie est donnée par
  \dltutompl{02}{part-3.py}
\end{info}

\end{document}
