\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}
\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newdnnexo{10}{Multi-layer neural networks}




\noindent At this step, we are able to classify linearly separable
data in multiple classes. Indeed, in many cases the related data are
not fully linearly separable. In such problems, multi-layer neural
networks can be used.

To illustrate non-linearly separable data problems, we will use
here another data set generator. Instead of the
\texttt{datasets.make\_blobs} function we will use the
\texttt{datasets.make\_moons} function.
\begin{manip}
  To test it by yourself, replace the \texttt{datasets.make\_blobs}
  function by the \texttt{datasets.make\_moons} function in the previous tutorial step.
\end{manip}

As a result, it should give the following plot:
\begin{center}
  \includegraphics[width=.6\linewidth]{moon}
\end{center}
As you can see, this data set is not linearly separable. So, our
Perceptron algorithm is not able to classify correctly this kind of
data.  To solve this problem, we should implement multi-layer dense
neural networks. This kind of networks is able to deal with
non-linear problems.

In fact, our mono-neural Perceptron algorithm can be seen as a
fundamental brick for building multi-layer neural networks able to deal
with complex problems.  Here, we are going to build a two layers dense
neural network to solve this particular problem. Now, we will draw a
neuron as follow:
\begin{center}
  \svg{0.75\linewidth}{./img/neuron.pdf_tex}
\end{center}
Here, a neuron is drawn as a circle that contains the transfer function
$a(x)$ and the activation function $z(a)$. A neuron accepts multiple inputs and
gives one output. To solve our non-linear classification problem with
the \textit{moon} data set, we will implement the following
architecture:
\begin{center}
  \svg{0.75\linewidth}{./img/multi-layer.pdf_tex}
\end{center}
\begin{itemize}
\item The input layer $\mathbf{x}$ is connected to three neurons. This neuron layer
  is called \textit{hidden} layer.
\item The \textit{hidden} layer neurons are connected to one neuron
  called \textit{output} layer.
\end{itemize}
This network forms a \textit{dense multi-layer} neural network able to
deal with non-linear separable data set. A special attention must be
given to the number of related weights and biases. In a general manner,
imagine a layer of rank $l-1$ densely connected to its next layer of
rank $l$. Now, suppose that the number of neurons that compose the
layer of rank $l$ is noted $N_l$. By the way, the total number of
weight $N_{(l-1) \rightarrow (l)}$ that connect the layer of rank $(l-1)$ to its next
one $(l)$ is:
\begin{equation}
  N_{(l-1) \rightarrow (l)} = N_{l-1} \times N_{l}\nonumber
\end{equation}
If we apply this simple formula to our architecture, the numbers of weight for each connection are:
\begin{align}
  N_{0 \rightarrow 1} &= N_{0} \times N_{1} = 2\times 3 = 6\nonumber \\
  N_{1 \rightarrow 2} &= N_{1} \times N_{2} = 3\times 1 = 3\nonumber
\end{align}
Instead of storing the weights as vector, a more practical data storage involves
matrices. So, these weights can be written as matrices where
:
\begin{itemize}
\item the length of the first axis is the number of neuron $N_{l-1}$ of its
  entry layer and
\item the length of the second axis is the number of neuron $N_l$ of the output  layer.
\end{itemize}
In the above case, it gives:
\begin{equation}
  \mathbf{W}_{1} = 
  \begin{bmatrix}
    w_{1_{11}}  & w_{1_{21}}  & w_{1_{31}} \\
    w_{1_{12}}  & w_{1_{22}}  & w_{1_{32}}
  \end{bmatrix}
  \quad \text{and}\quad \mathbf{W}_{2} =
  \begin{bmatrix}
    w_{2_{11}}  \\
    w_{2_{12}}  \\
    w_{2_{13}} 
\end{bmatrix}\nonumber
\end{equation}

\begin{info}
  Note on mathematical conventions:
  \begin{itemize}
  \item scalars are written as italic-lowercase typeface, e.g,  $w$
  \item vectors are written as bold-lowercase typeface, e.g, $\mathbf{w}$
  \item matrices or tensors are written as italic-uppercase typeface, e.g, $\mathbf{W}$
  \end{itemize}
\end{info}

In general manner, weight matrices $\mathbf{W}_{l}$ (from layer $l-1$ to layer $l$) are written as:
\def\matriximg{%
  \begin{matrix}
    w_{l_{11}} & w_{l_{21}} & \cdots & \cdots &w_{l_{k1}}  \\
    w_{l_{12}} & w_{l_{22}} & \cdots & \cdots &w_{l_{k2}}  \\
    \vdots     & \vdots     & \cdots & \cdots &\vdots      \\
    w_{l_{1j}} & w_{l_{2j}} & \cdots & \cdots & w_{l_{kl}}  \\
   \end{matrix}
}%
\begin{equation}
  \text{\scriptsize $l$ rows = entry number $N_{l-1}$}\left\{\left[\vphantom{\matriximg}\right.\right.\kern-2\nulldelimiterspace
  \overbrace{\matriximg}^{\text{\scriptsize $k$ col = output number $N_{l}$}}\kern-\nulldelimiterspace\left.\vphantom{\matriximg}\right] = \mathbf{W}_{l}\nonumber
\end{equation}
Now, we will show how to implement the feed forward step with matrices.

\section*{Part 1 : the feed forward step}
We will move on higher level of abstraction. A more convenient
and general point of view is adopted by using matrices instead of
vectors or scalars. Here, each layer can be considered as a single box where inputs and
outputs are highlighted by arrows. With this new visualization flow chart, the
previous neural network scheme becomes:
\begin{center}
  \svg{1\linewidth}{./img/multi-layer-2.pdf_tex}
\end{center}
Here, single neurons are hidden; all the symbols are considered as
matrices instead of scalars and vectors. Let's detail the matrix shapes
associated to each symbols of the above diagram. For the current
example it gives the following shapes.
\begin{itemize}
\item $\mathbf{X}$ is a $(2\times m)$ matrix because it contains the $m$ points of the mini-batch as follows:
    \begin{equation}
  \mathbf{X} = 
  \begin{bmatrix}
    x_{1}^{(1)}   & x_{2}^{(1)} \\
    \vdots  & \vdots \\
    x_{1}^{(m)}   & x_{2}^{(m)}  \\
  \end{bmatrix}
\end{equation}
%   \def\matriximg{%
%   \begin{matrix}
%     x_{11} & x_{21}  \\
%     x_{12} & x_{22}  \\
%     \vdots & \vdots  \\
%     x_{1m} & x_{2m}  \\
%    \end{matrix}
% }%
%   \begin{equation}
%   \text{\scriptsize $m$ entry points}\left\{\left[\vphantom{\matriximg}\right.\right.\kern-2\nulldelimiterspace
%   \overbrace{\matriximg}^{\text{\scriptsize entry coordinates}}\kern-\nulldelimiterspace\left.\vphantom{\matriximg}\right] = \mathbf{X}\nonumber
% \end{equation}

\item $\mathbf{A_1}$ and $\mathbf{A_2}$ are respectively $(m\times 3)$ and $(m\times 1)$ matrices as follows:
  \begin{equation}
  \mathbf{A}_{1} = 
  \begin{bmatrix}
    a_{1_{1}}^{(1)}  & a_{1_{2}}^{(1)} & a_{1_{3}}^{(1)}\\
    \vdots    & \vdots   & \vdots \\
    a_{1_{1}}^{(m)}  & a_{1_{2}}^{(m)}  & a_{1_{3}}^{(m)} \\
  \end{bmatrix}
  \quad \text{and}\quad \mathbf{A}_{2} =
  \begin{bmatrix}
    a_{2}^{(1)} \\ 
    \vdots \\
    a_{2}^{(m)} 
\end{bmatrix}\nonumber
\end{equation}

\item $\mathbf{Z_1}$ and $\mathbf{Z_2}$ are respectively $(m \times 3)$ and $(m\times 1)$ matrices as follows:
  \begin{equation}
  \mathbf{Z}_{1} = 
  \begin{bmatrix}
    z_{1_{1}}^{(1)}  & z_{1_{2}}^{(1)}  & z_{1_{3}}^{(1)} \\
    \vdots    & \vdots   & \vdots \\
    z_{1_{1}}^{(m)}  & z_{1_{2}}^{(m)}  & z_{1_{3}}^{(m)} \\
  \end{bmatrix}
  \quad \text{and}\quad \mathbf{Z}_{2} =
  \begin{bmatrix}
    z_{2}^{(1)} \\ 
    \vdots \\
    z_{2}^{(m)} 
\end{bmatrix}\nonumber
\end{equation}

\item $\mathbf{W_1}$ and $\mathbf{W_2}$ are respectively $(2\times 3)$ and $(3\times 1)$ matrices as follows:
\begin{equation}
  \mathbf{W}_{1} = 
  \begin{bmatrix}
    w_{1_{11}}  & w_{1_{21}}  & w_{1_{31}} \\
    w_{1_{12}}  & w_{1_{22}}  & w_{1_{32}}
  \end{bmatrix}
  \quad \text{and}\quad \mathbf{W}_{2} =
  \begin{bmatrix}
    w_{2_{11}}  \\
    w_{2_{12}}  \\
    w_{2_{13}} 
\end{bmatrix}\nonumber
\end{equation}


\item $\mathbf{B_1}$ and $\mathbf{B_2}$ are respectively $(1\times 3)$ and $(1\times 1)$ matrices as follows:
\begin{equation}
  \mathbf{B}_{1} = 
  \begin{bmatrix}
    b_{1_{1}}  & b_{1_{2}}  & b_{1_{3}}
  \end{bmatrix}
  \quad \text{and}\quad \mathbf{B}_{2} =
  \begin{bmatrix}
    b_{2} 
\end{bmatrix}\nonumber
\end{equation}
\end{itemize}

Let's go back to the code. Following the previous comments, the weights can be initialized as
matrices: \vspace*{-1\baselineskip}
\begin{lstlisting}
W1 = np.random.random( (2, 3) )
W2 = np.random.random( (3, 1) )
\end{lstlisting}
The length of the first axis corresponds to the number of entry and
the length of the second axis corresponds to the number of
output  of the considered layer. Bias can be initialized as: \vspace*{-1\baselineskip}
\begin{lstlisting}
B1 = np.zeros( (1, 3) )
B2 = np.zeros( (1, 1) )
\end{lstlisting}
You can note here that the first axis length of bias is always equal
to one because there is only one bias by neuron.


\subsection*{Let's code!}

\begin{manip}
  Thanks to the first tutorial, create a Python environment \texttt{(dnn)} and launch the \textbf{thonny} editor.
\end{manip}

\begin{manip}
  Download the
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/dnn/mltool.py}{\texttt{mltool.py}}
   file and put it in your current working dir.
\end{manip}

\begin{manip}
  Download the \dltutodnn{10}{tuto-10.py} script file and
  execute it with \textbf{thonny}. This file will be used as starting point for
  this tutorial. 
\end{manip}

\begin{task}
  Edit this file by implementing the feed forward step 
\end{task}

\begin{info}
  The correction of this part is given by \dltutodnn{10}{tuto-10\_correction-part1.py}
\end{info}


\section*{Part 2: the backpropagation step}
The most difficult part of multi-layer neural networks is the
implementation of the back propagation step through the gradient
descent algorithm. We take here a modified MSE loss function that gives:
\begin{equation}
  L(\mathbf{Z}_2) = \frac{1}{2} \sum\limits_{i=1}^{m} \left( z_{2}^{(i)} - t^{(i)} \right)^2\nonumber
\end{equation}
In the above case, the gradient of the loss function regarding $\mathbf{W}_1$ and $\mathbf{W}_2$  give the following tensors:
\begin{align}
   \nabla L(\mathbf{W}_{1}) &= 
  \begin{bmatrix}
    \frac{\partial L}{\partial w_{1_{11}}}  &  \frac{\partial L}{\partial w_{1_{21}}}  &  \frac{\partial L}{\partial w_{1_{31}}} \\ 
    \frac{\partial L}{\partial w_{1_{12}}}  &  \frac{\partial L}{\partial w_{1_{22}}}  &  \frac{\partial L}{\partial w_{1_{32}}}
  \end{bmatrix}\label{eq:w_12}  \\
   \nabla L(\mathbf{W}_{2}) &= \begin{bmatrix}
   \frac{\partial L}{\partial w_{2_{11}}}  & 
    \frac{\partial L}{\partial w_{2_{12}}} & 
    \frac{\partial L}{\partial w_{2_{13}}} \end{bmatrix}\nonumber 
\nonumber
\end{align}

\subsection*{Part 2.1:  backpropagation of the second weight layer $\mathbf{W}_{2}$}

Now, we focus on the derivative of the loss regarding $\mathbf{W}_{2}$. For the first component of $\nabla L(\mathbf{W}_{2})$, it gives:
\begin{align}
  \frac{\partial L}{\partial w_{2_{11}}}
  &=  \sum\limits_{i=1}^{m} \frac{\partial}{\partial w_{2_{11}}}\left( \frac{1}{2} \left( z_{2}^{(i)} - t^{(i)}\right)^2\right)\label{eq:delta}
\end{align}
So, the following derivative chain rule can be written:
\begin{align}
  \frac{\partial f}{\partial w_{2_{11}}} = 
  \frac{\partial  f}{\partial  z_2}\cdot 
  \frac{\partial  z_2}{\partial  a_2}\cdot  
  \frac{\partial  a_2}{\partial w_{2_{11}}} \quad \text{where} \quad
  f(z_2) = \frac{1}{2}\left( z_2 - t\right)^2\nonumber
\end{align}
It gives:
\begin{align}
  \frac{\partial f}{\partial w_{2_{11}}} = \left( z_2 - t\right)\cdot \sigma '(a_2)\cdot z_{1_{1}}\nonumber
\end{align}
So, when retrieving the sum, it gives:
\begin{align}
  \frac{\partial L}{\partial w_{2_{11}}} = \sum\limits_{i=1}^{m} \left( z_{2}^{(i)} - t^{(i)}\right)\cdot \sigma '(a_{2}^{(i)})\cdot z_{1_{1}}^{(i)}\nonumber
\end{align}

% Finally, the above formula can be written in a vectorial form as:
% \begin{align}
%   \boxed{\nabla L(\mathbf{w}_{2})  = \sum\limits_{i=1}^{m} \left( \mathbf{z}_{2_i} - \mathbf{t}_i\right)\cdot \sigma '(\mathbf{a}_{2_{i}})\cdot \mathbf{z}_{1_i}}\nonumber
% \end{align}
% % It can be implemented as follows:
% \begin{align}
%   \mathbf{w}_{2}  \rightarrow \mathbf{w}_{2} - \eta\sum\limits_{i=1}^{m} \underbrace{\left( \mathbf{z}_{2_i} - \mathbf{t}_i\right)}_{\text{\texttt{A2}}}\cdot \underbrace{\sigma '(\mathbf{a}_{2_{i}})}_{\text{\texttt{B2}}}\cdot \underbrace{\mathbf{z}_{1_i}}_{\text{\texttt{C2}}}\nonumber
% \end{align}
Finally, the gradient descent algorithm could be written in a tensorial form as:
\begin{align}
  \mathbf{W}_{2}  \rightarrow \mathbf{W}_{2} - \eta\left[ \mathbf{Z}_{1}^\intercal \cdot \left(\underbrace{\left( \mathbf{Z}_{2} - \mathbf{T}\right)}_{\text{\texttt{L2}}}\circ \underbrace{\sigma '(\mathbf{A}_{2})}_{\text{\texttt{M2}}} \right)  \right]\nonumber
\end{align}

\subsection*{Let's code}

\begin{task}
  From the above equation, implement the backpropagation of the
  second weight layer $\mathbf{W}_{2}$. 
\end{task}

\begin{task}
  Implement also the backpropagation of the second bias layer
  $\mathbf{B}_{2}$.  Note that you can use the \keyw{np.sum()} function (\href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/latex/tuto-dnn-from-scratch-en_06.pdf}{see tutorial \#6} for more details).
\end{task}


\begin{info}
  The correction of this part is given by \dltutodnn{10}{tuto-10\_correction-part2-1.py}
\end{info}


\subsection*{Part 2.2:  backpropagation of the hidden weight layer $\mathbf{W}_{1}$}
In a similar way, the derivative chain can be applied to $\mathbf{W}_{1}$ as:
\begin{align}
  \frac{\partial f}{\partial w_{1_{11}}} = 
  \frac{\partial  f}{\partial  z_2}\cdot 
  \frac{\partial  z_2}{\partial  a_{2}} \cdot  
  \frac{\partial  a_2}{\partial  z_{1_1}} \cdot  
  \frac{\partial  z_{1_1}}{\partial a_{1_1}} \cdot
  \frac{\partial  a_{1_1}}{\partial w_{1_{11}}} \quad \text{where} \quad
  f(z_2) = \frac{1}{2}\left( z_2 - t\right)^2\nonumber
\end{align}
Be aware, in the above formula, as $w_{1_{11}}$ is an input of the
first neuron of the hidden layer, the chain rule must contain the
related activation function $z_{1_1}$ and $a_{1_1}$. So, the above formula
gives:
\begin{align}
  \frac{\partial f}{\partial w_{1_{11}}} = \left( z_2 - t\right)\cdot \sigma '(a_2)\cdot w_{2_{11}} \cdot \sigma '(a_{1_1})
  \cdot x_{1}
  \nonumber
\end{align}
Again, this derivative chain rule must be included in the sum as:
\begin{align}
  \frac{\partial L}{\partial w_{1_{11}}} = \sum\limits_{i=1}^{m} \left( z_{2}^{(i)} - t^{(i)}\right)\cdot \sigma '(a_{2}^{(i)})\cdot \cdot w_{2_{11}} \cdot \sigma '(a_{1_{1}}^{(i)}) \cdot x_{1}^{(i)}
  \nonumber
\end{align}
% Finally, the above formula can be generalized as:
% \begin{align}
%    \boxed{\nabla l(\mathbf{w}_{1}) = \sum\limits_{i=1}^{m} \left( \mathbf{z}_{2}^{(i)} - t^{(i)}\right)\cdot \sigma '(\mathbf{a}_{2}^{(i)})\cdot \mathbf{w}_{2} \cdot \sigma '(\mathbf{a}_{1}^{(i)}) \cdot \mathbf{x}^{(i)}}
%   \nonumber
% \end{align}
% To operate correctly, the gradient $\nabla l(\mathbf{w}_{1})$ must be stored as a tensor (or a matrix) and this tensor must have similar dimension as $\mathbf{w}_{1}$ (see equation \ref{eq:w_12}). In the current example, it gives:
% \begin{align}
%    \nabla l(\mathbf{w}_{1}) = 
%   \begin{bmatrix}
%     \frac{\partial L}{\partial w_{1_{11}}}  &  \frac{\partial L}{\partial w_{1_{21}}}  &  \frac{\partial L}{\partial w_{1_{31}}} \\ 
%     \frac{\partial L}{\partial w_{1_{12}}}  &  \frac{\partial L}{\partial w_{1_{22}}}  &  \frac{\partial L}{\partial w_{1_{32}}}
%   \end{bmatrix}
%   \nonumber
% \end{align}

% It can be implemented as follows:
% \begin{align}
%   \mathbf{w}_{1}  \rightarrow \mathbf{w}_{1} - \eta\sum\limits_{i=1}^{m} \overbrace{\underbrace{\left( \mathbf{z}_{2_i} - \mathbf{t}_i\right)}_{\text{\texttt{A2}}}\cdot \underbrace{\sigma '(\mathbf{a}_{2_{i}})}_{\text{\texttt{B2}}}\cdot \mathbf{w}_{2}}^{\text{\texttt{A1}}}\cdot \overbrace{\sigma '(\mathbf{a}_{1_i})}^{\text{\texttt{B1}}} \cdot \overbrace{\mathbf{x}_{i}}^{\text{\texttt{C1}}}
%   \nonumber
% \end{align}
Finally, the gradient descent algorithm could be written in a tensorial form as:
\begin{align}
  \mathbf{W}_{1}  \rightarrow \mathbf{W}_{1} - \eta \left[\mathbf{X}^\intercal \cdot \left( \overbrace{\left(\left(\text{\texttt{L2}}\circ \text{\texttt{M2}}\right)  \cdot \mathbf{W}_{2}^\intercal \right)}^{\text{\texttt{L1}}} \circ \overbrace{\sigma '(\mathbf{A}_{1})}^{\text{\texttt{M1}}} \right)\right]
  \nonumber
\end{align}

\subsection*{Let's code}

\begin{task}
  From the above equation, implement the backpropagation of the
  first weight layer $\mathbf{W}_{1}$. 
\end{task}

\begin{task}
  Implement also the backpropagation of the first bias layer
  $\mathbf{B}_{1}$. Note that you can use the \keyw{np.sum()} function (
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/latex/tuto-dnn-from-scratch-en_06.pdf}{see tutorial \#6} for more details).
\end{task}


\begin{info}
  The correction of this part is given by \dltutodnn{10}{tuto-10\_correction-part2-2.py}
\end{info}



\section*{Conclusion}
The full implementation of the multi-layer neural network able to deal
with non-linear separable data is given by
\dltutodnn{10}{tuto-10\_correction-part2-2.py}. As you can see in the
following chart, the provided algorithm is able to classify non-linear
separable data provided by the \texttt{make\_moons} function.
\begin{center}
  \includegraphics[width=.6\linewidth]{multi-layer-full}
\end{center}

\begin{manip}
 To train yourself, you can try to add the following features to the previous code:
 \begin{enumerate}
 \item monitoring of the \textit{loss} versus \textit{epoch}
 \item change the \texttt{make\_moons} generator by \texttt{make\_circle}. You must obtain something like this:
   \begin{center}
     \includegraphics[width=.8\linewidth]{circle}
   \end{center}
 \end{enumerate}
\end{manip}

\begin{info}
  Congratulation! You finish this tutorial sequence! You have coded from scratch a dense neural network able to deal with complex data! 
\end{info}

\end{document}
