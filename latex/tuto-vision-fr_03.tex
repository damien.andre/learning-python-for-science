\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Vision par ordinateur}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newvisionexo{03}{Reconnaissance de grain (partie 1)}

\noindent L'image suivante est une image "synthétique" issue d'une
simulation qui montre des grains circulaires de différents diamètres
répartis dans l'espace.  L'objectif de cet exercice est d'extraire de
cette image des données statistiques en termes de distribution des
diamètres (aussi appelés répartition granulométrique). Dans cet
exercice, nous utiliserons le module
\href{https://docs.scipy.org/doc/scipy/reference/ndimage.html}{\texttt{ndimage}}
de la bibliothèque
\href{https://docs.scipy.org/doc/scipy/}{\texttt{scipy}} qui donne
accès à des algorithmes avancés de traitement d'image.

\begin{center}
  \includegraphics[width=0.6\linewidth]{../script/vision/tuto-03/grain-dem-img.png}
\end{center}

\subsection*{Partie 1 : traitement préliminaire}

\begin{manip}
  Téléchargez l'image \dltutovision{03}{grain-dem-img.png} et placez-la dans votre répertoire courant 
\end{manip}

\begin{task}
  En vous basant sur l'exercice précédant, réalisez les traitements suivants :
  \begin{enumerate}
  \item chargez l'image avec \keyw{imread(...)}
  \item convertissez cette image en niveau de gris avec \keyw{rgb2gray(...)}
  \item appliquez un seuillage (affichage noir et blanc)
  \item affichez l'image 
  \end{enumerate}
\end{task}

\begin{info}
"Mettez au chaud" l'image issue de cette question, elle sera également utilisée dans la partie 3 !
\end{info}

\begin{info}
  Nous allons maintenant nous intéresser au grain dans le coin
  inférieur droit de l'image.
\end{info}

\begin{task}
  À l'aide de la technique du slicing (voir exercice précédant), isolez ce grain.
  Vous devriez obtenir l'image suivante : \dltutovision{03}{grain-dem-img\_correction-part1.svg}
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{03}{grain-dem-img\_correction-part1.py}
\end{info}

\subsection*{Partie 2 : traitement sur un seul grain}
Nous allons maintenant extraire automatiquement les coordonnées du
centre du grain et son rayon. Pour cela, nous utiliserons les
algorithmes de traitement d'image \keyw{ndimage} de la bibliothèque \keyw{scipy}.

\begin{manip}
  Importez le module
  \href{https://docs.scipy.org/doc/scipy/reference/ndimage.html}{\texttt{ndimage}}
  de la bibliothèque
  \href{https://docs.scipy.org/doc/scipy/}{\texttt{scipy}} :
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
from scipy import ndimage
\end{lstlisting}
\end{manip}

\begin{task}
  À l'aide de la fonction
  \href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.center_of_mass.html}{\texttt{ndimage.center\_of\_mass(...)}},
  déterminez les coordonnées du centre du grain.
\end{task}

\begin{task}
  Afin de vérifier que vous avez bien obtenu les bonnes coordonnées,
  dessinez une croix grise de 3 par 3 pixels au centre de ce grain (voir figure 
  \dltutovision{03}{grain-dem-img\_correction-part2-1.svg}).
\end{task}

\begin{info}
  Nous allons maintenant tenter de trouver la "boite englobante" de ce
  grain. Une boite englobante (bounding box en anglais) est simplement
  la forme rectangulaire qui contient le grain (voir figure
  \dltutovision{03}{grain-dem-img\_correction-part2-2.svg}).
\end{info}

\begin{task}
En vous aidant de l'algorithme suivant :
 \vspace*{-1.5\baselineskip}
\begin{lstlisting}
import numpy as np
x_array = np.nonzero(np.argmax(grain_img, axis=1))[0]
y_array = np.nonzero(np.argmax(grain_img, axis=0))[0]
xmin = np.min(x_array)
ymin = np.min(y_array)
\end{lstlisting}
 \vspace*{-.5\baselineskip}
 qui permet de retrouver les coordonnées du coin supérieur gauche
 \keyw{(xmin, ymin)} de la boite englobante, déterminez également les
 coordonnées du coin inférieur droit \keyw{(xmax, ymax)}.
\end{task}

\begin{task}
  Afin de vérifier le résultat précédant, dessinez en gris les pixels correspondants au coin supérieur gauche et au 
  coin inférieur droit de la boite englobante (voir figure \dltutovision{03}{grain-dem-img\_correction-part2-3.svg}).
\end{task}

\begin{task}
  À l'aide du \textit{slicing}, dessinez maintenant en gris la boite
  englobante telle que le montre la figure suivante
  \dltutovision{03}{grain-dem-img\_correction-part2-2.svg}
\end{task}

\begin{task}
  À l'aide des résultats précédants, déterminez le rayon (en pixel) du
  grain. Affichez la valeur du rayon avec la fonction \keyw{print(...)}.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{03}{grain-dem-img\_correction-part2.py}
\end{info}


\subsection*{Partie 3 (optionnelle) : traitement automatisé sur tous les grains}
Dans cette partie, nous allons automatiser la procédure de la partie 2
sur l'ensemble des grains. Pour cela, nous devons tout d'abord
identifier (ou étiqueter) chacun des grains sur l'image à l'aide de la
fonction
\href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.label.html}{\texttt{ndimage.label(...)}}.

\begin{task}
  Afin de comprendre ce que fait cette fonction, appliquez-la sur
  la matrice \texttt{a} suivante : \vspace*{-1.5\baselineskip}
\begin{lstlisting}
a = np.array([[0,0,1,1,0,0],
              [0,0,0,1,0,0],
              [1,1,0,0,1,0],
              [0,0,0,1,0,0]])
res, N = ndimage.label(a)
\end{lstlisting}
  \vspace*{-.5\baselineskip}
   Observez les résultats renvoyés (\keyw{res} et \keyw{N}). D'après vous, à quoi correspondent-ils ?
\end{task}

\begin{task}
Appliquez cette fonction sur l'image issue de la \textit{task 1}. Combien y'a-t-il de grains sur cette image ?
\end{task}


\begin{task}
  Imaginez et implémentez un algorithme qui balaye chacun de ces grains et qui :
  \begin{itemize}
  \item dessine une croix au centre de chacun d'entre eux
  \item dessine la boite englobante de chacun d'entre eux.
  \end{itemize}
  Vous devriez obtenir l'image suivante
  \dltutovision{03}{grain-dem-img\_correction-part3.svg}
\end{task}

\begin{task}
  Déterminez le rayon moyen en pixel des grains (pour cela, vous
  pouvez vous aider de la fonction
  \href{https://numpy.org/doc/stable/reference/generated/numpy.mean.html}{\texttt{np.mean(...)}}).
\end{task}

\begin{task}
  Affichez, à l'aide de la bibliothèque \texttt{matplotlib}, un histogramme qui montre la distribution des rayons des grains tel que le montre la figure suivante
  \dltutovision{03}{grain-dem-img\_correction-part3-hist.svg}
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{03}{grain-dem-img\_correction-part3.py}
\end{info}

\begin{info}
  La correction de tout l'exercice est donnée par \dltutovision{03}{grain-dem-img\_correction.py}
\end{info}

\end{document}
