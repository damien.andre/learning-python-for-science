\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}
\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newdnnexo{08}{Stochastic gradient descent with mini-batch}

\noindent When the amount of data is very large, it may be difficult to treat
all the data in one step. To avoid memory problem, the data could be
sliced in $k$ mini-batches. The following picture illustrates the
mini-batch process. Here the data are sliced in two separated mini-batches.
\begin{center}
  \includegraphics[width=.6\linewidth]{data-mini-batch}
\end{center}
Another good reason for using mini-batches is for optimizing the
gradient descent algorithm. As you know, gradient descent algorithm
can be trapped in local minima. Using mini-batch allows to introduce
\textit{noise} in the algorithm. It increases the probality to find the
global minima instead of a local one. This process is called \textit{stochastic gradient
  descent algorithm}. It is simple, elegant and really efficient!
This strategy is massively used in machine learning. It consists in:
\begin{enumerate}
\item randomizing the data storage. It means that data arrays are shuffled.
\item slicing the newly organized data and label. 
\end{enumerate}

\section*{Shuffling data}
Since the beginning of this document, we have seen that data are stored in two arrays:
\begin{enumerate}
\item the \texttt{data} array that contains the entry point and
\item the \texttt{label} array that contains the target values.
\end{enumerate}
The shuffling operation consists in reordering randomly these arrays. Indeed,
special attention must be taken. The shuffling operation must keep the
correspondence between the indices of the \texttt{data} and the \texttt{label}
arrays. The next figure summarizes this process. 
\begin{center}
  \svg{0.75\linewidth}{./img/shuffle.pdf_tex}
\end{center}
Again, Numpy gives all we need! Let's make a sample code into the
interpreter: \vspace*{-1\baselineskip}
\begin{lstlisting}
>>> data = np.arange(0,100,10)
array([ 0, 10, 20, 30, 40, 50, 60, 70, 80, 90])
>>> label = np.arange(0,1000,100)
array([  0, 100, 200, 300, 400, 500, 600, 700, 800, 900])
\end{lstlisting}
Now, we create a new array that contains the index of the
\texttt{data} and \texttt{label} arrays:
\vspace*{-1\baselineskip}
\begin{lstlisting}
>>> s = np.arange(data.shape[0])
array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
\end{lstlisting}
Let's shuffling this array thanks to the \texttt{np.random.shuffle} function:
\vspace*{-1\baselineskip}\begin{lstlisting}
>>> np.random.shuffle(s)
>>> s
array([7, 0, 9, 5, 8, 4, 2, 6, 1, 3]) 
\end{lstlisting}
Now, we can use this shuffled arrays as indices for re-organizing the
\texttt{data} and \texttt{label} arrays as follow:
\vspace*{-1\baselineskip}\begin{lstlisting}
>>> data[s]
array([70 , 0, 90 , 50 , 80 , 40 , 20 , 60 , 10 , 30])
>>> label[s]
array([700, 0, 900, 500, 800, 400, 200, 600, 100, 300])
\end{lstlisting}
As you can see both \texttt{data} and \texttt{label} has been
reorganized with the same indices. It is very important, because we
need to keep the correspondence between the 
\texttt{data} and the \texttt{label} arrays.

\section*{splitting data}

After shuffling data, now, we want to slice these 
arrays in a given number of mini-batches. We can use the \texttt{np.split} function as:
\vspace*{-1\baselineskip}\begin{lstlisting}
>>> mb = 5 # number of mini-batch 
>>> data_s = np.split(data[s], mb)                                                                                                  
[array([70,  0]), array([90, 50])  , array([80, 40])  , array([20, 60])  , array([10, 30])]
>>> label_s = np.split(label[s], mb)                                                                                                
[array([700, 0]), array([900, 500]), array([800, 400]), array([200, 600]), array([100, 300])]
\end{lstlisting}
Here, we have built five mini-batches with shuffled
data. 

\section*{Let's code!}

\begin{manip}
  Thanks to the first tutorial, create a Python environment \texttt{(dnn)} and launch the \textbf{thonny} editor.
\end{manip}

\begin{manip}
  Download the
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/dnn/mltool.py}{\texttt{mltool.py}}
   file and put it in your current working dir.
\end{manip}

\begin{manip}
  Download the \dltutodnn{08}{tuto-08.py} script file and
  execute it with \textbf{thonny}. This file will be used as starting point for
  this tutorial. Note that this file is simply the correction of the previous tutorial.
\end{manip}

\begin{task}
  Edit this script file for implementing the stochastic gradient descent with mini-batch
\end{task}

\begin{info}
  The correction of this part is given by \dltutodnn{08}{tuto-08\_correction-part1.py}
\end{info}

\section*{Conclusion}

The related algorithm gives the following evolution of loss (left image).
\begin{center}
  \svg{.48\linewidth}{./img/loss-stoch.pdf_tex}
  \svg{.48\linewidth}{./img/loss.pdf_tex}
\end{center}
As you can see, the evolution is a little bit noisy but the stochastic
gradient descent method convergence rate is faster than the
non-stochastic one!  The next tutorial step will give you a last trick
for increasing (again) the convergence rate.



\end{document}
