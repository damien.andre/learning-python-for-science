\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}
\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newdnnexo{07}{A vectorized Perceptron}

\noindent We stated, from the previous tutorial step, that optimal
values of weights and bias can be found thanks to the back propagation
step that implements the gradient descent algorithm. This algorithm
gives:
\begin{align}
  w_0 &\rightarrow w_0 +  \frac{2\eta}{m} \sum\limits_{i=1}^{m} (y^{(i)} - t^{(i)})\cdot \sigma '(a^{(i)})\cdot x_{0}^{(i)} \nonumber\\ 
  \vdots \nonumber\\
  w_n &\rightarrow w_n +  \frac{2\eta}{m} \sum\limits_{i=1}^{m} (y^{(i)} - t^{(i)})\cdot \sigma '(a^{(i)})\cdot x_{n}^{(i)} \nonumber\\ 
  b   &\rightarrow b +  \frac{2\eta}{m} \sum\limits_{i=1}^{m} (y^{(i)} - t^{(i)})\cdot \sigma '(a^{(i)})  \nonumber
\end{align}
For the weight part, the related formula can be expressed in a vectorial form as:
\begin{equation}
 \mathbf{w} \rightarrow \mathbf{w} +  \frac{2\eta}{m} \sum\limits_{i=1}^{m} (y^{(i)} - t^{(i)})\cdot \sigma '(a^{(i)})\cdot \mathbf{x}^{(i)} \nonumber
\end{equation}
where $\mathbf{w}$ and $\mathbf{x}^{(i)}$ are $n$-dimensional
vectors. Now, as you can see in the above formula, we must implement a
summation on all the $m$ points. Based on this observation, the sum
can be replaced thanks to \textit{linear algebra} operation such as the
\textit{dot} products:
\begin{equation}
  \mathbf{w} \rightarrow \mathbf{w} +  \frac{2\eta}{m} \left(\mathbf{X}^\top   \cdot 
    \left[ \left(\mathbf{y} - \mathbf{t} \right)\circ
      \sigma'\left(\mathbf{a}\right) \right]
    \right)\nonumber
\end{equation}
where:
\begin{itemize}
\item $\mathbf{w}$, $\mathbf{y}$, $\mathbf{t}$ and $\mathbf{a}$ are $(n)$-dimensional vectors,
\item $\mathbf{X}$ is a $(n\times m)$-dimensional \textbf{matrix} (or tensor),
\item $^\top$ is the transpose of a matrix (or a vector), 
\item $\circ$ is the \textit{Hadamard} product (the so-called \textit{element wise} product) 
\item $\cdot$ is the dot product (here, the dot product replaces the sum), 
\item $n$ is the number of dimension of an entry $\mathbf{x}_i$ and 
\item $m$ is the total number of entry  $\mathbf{X}$.
\end{itemize}

\begin{info}
  Here, the advantage of using linear algebra (tensorial) operations such as
  \textit{Hadamard} or \textit{dot} products is to deal with large
  amount of data. Linear algebra computations involved in tensorial
  operations have been optimized for a long time and the main linear algebra algorithms
  can be massively parallelized in both CPU and GPU. Deep learning processes
  extensively use linear algebra computations. For example, the name
  of one of the must popular free deep learning framework (supported by
  Google) is ``\textit{TensorFlow}''. For now, \textbf{be aware}, to
  follow the next part of this document, you must be familiar with
  tensorial operations and linear algebra computation rules.
\end{info}

To clarify this last formula, we will take the first example of this
document that implements the following data:
\begin{center}
  \includegraphics[width=.6\linewidth]{data-label1}
\end{center}
Here, the number of dimension $n$ of an entry $\mathbf{x}_i$ is 2 and
the total number of entry $\mathbf{X}$ is 1,000. To summarize, in this
example, $n=2$ and $m=1,000$. Now, let's focus on the trick able to
replace a \textit{sum} by a \textit{dot product}. If we take the
current example, we can highlights the main terms of the previous
equation:
\begin{equation}
  \underbrace{\mathbf{w}}_{\begin{bmatrix}
  w_0  \\ w_1 \end{bmatrix}} \rightarrow \underbrace{\mathbf{w}}_{\begin{bmatrix}
  w_0  \\ w_1 \end{bmatrix}} +  \overbrace{\frac{2\eta}{m}}^{\text{scalar}} \times  \underbrace{\mathbf{X}^\top}_{\begin{bmatrix}
      x_{0_{0}}  & x_{0_{1}} & \dots & x_{0_{999}}\\  x_{1_{0}}  & x_{1_{1}} & \dots & x_{1_{999}}\end{bmatrix}
    } \cdot
    \underbrace{\left[ \left(\mathbf{y} - \mathbf{t} \right)\circ
      \sigma'\left(\mathbf{a}\right) \right]}_{\begin{bmatrix}
      a_0  \\ a_1 \\ \vdots \\ a_{999} \end{bmatrix}} 
    \nonumber
\end{equation}
However, it does not correspond to the internal way of vector storage
in Numpy. Numpy stores vectors as row vectors. So, the previous formula
has to be rewritten as follows:
\begin{equation}
  \underbrace{\mathbf{w}}_{\begin{bmatrix}
  w_0  & w_1 \end{bmatrix}} \rightarrow \underbrace{\mathbf{w}}_{\begin{bmatrix}
  w_0  & w_1 \end{bmatrix}} +  \overbrace{\frac{2\eta}{m}}^{\text{scalar}} \times
    \underbrace{\left[ \left(\mathbf{y} - \mathbf{t} \right)\circ
      \sigma'\left(\mathbf{a}\right) \right]^\top}_{\begin{bmatrix}
      a_0  & a_1 & \dots & a_{999} \end{bmatrix}} \cdot \underbrace{\mathbf{X}}_{\begin{bmatrix}
      x_{0_{0}}  & x_{0_{1}} \\  x_{1_{0}}  & x_{1_{1}} \\ \vdots &  \vdots \\ x_{999_{0}}  & x_{999_{1}}\end{bmatrix}
    }
    \nonumber
\end{equation}

So, the gradient descent algorithm in Python can be written as:
\begin{equation}
  \boxed{
    \mathbf{w} \rightarrow \mathbf{w} +  \frac{2\eta}{m} \times
    \left[ \left(\mathbf{y} - \mathbf{t} \right)\circ
      \sigma'\left(\mathbf{a}\right) \right]^\top
    \cdot \mathbf{X}}
  \nonumber
\end{equation} 

The next two lines show the Python's implementation for the weight vector \keyw{w} and bias scalar \keyw{b}:
 \vspace*{-1.5\baselineskip}
\begin{lstlisting}
w += -(2./m)*eta * np.dot(((y-t)*sigmoid_prime(a)).T, x)
b += -(2./m)*eta * np.sum( (y-t)*sigmoid_prime(a))
\end{lstlisting}
\vspace*{-.5\baselineskip} where \keyw{y}, \keyw{t}, \keyw{a} are vectors whereas \keyw{x} is a matrix.


\section*{Let's code!}

\begin{manip}
  Thanks to the first tutorial, create a Python environment \texttt{(dnn)} and launch the \textbf{thonny} editor.
\end{manip}

\begin{manip}
  Download the
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/dnn/mltool.py}{\texttt{mltool.py}}
   file and put it in your current working dir.
\end{manip}

\begin{manip}
  Download the \dltutodnn{07}{tuto-07.py} script file and
  execute it with \textbf{thonny}. This file will be used as starting point for
  this tutorial.
\end{manip}

\begin{task}
  Complete this script file for implementing the vectorized version of the Perceptron 
\end{task}

\begin{info}
  The correction of this part is given by \dltutodnn{07}{tuto-07\_correction-part1.py}
\end{info}

\section*{Conclusion}

The related correction code records the evolution of the loss
function into the \texttt{loss} array. This is very important. It
allows to check if the learning process is going right! The following
chart shows an example of a ``good'' loss evolution.
\begin{center}
  \svg{.5\linewidth}{./img/loss.pdf_tex}
\end{center}
As you can see the loss decreases continuously toward zero while the
epoch number increases. It means that the back propagation algorithm
is working as expected: it minimizes the loss by adjusting weights and
bias values in a correct way thanks to the gradient descent algorithm.
The next tutorial will show some techniques to speed-up this
convergence rate.




\end{document}
