\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}
\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newdnnexo{05}{The gradient descent algorithm}

\noindent As illustrated in two dimensions on the next figure, the gradient
descent algorithm is an iterative algorithm able to find the minimum
of a derivable function $f(x)$.
\begin{center}
  \svg{.6\linewidth}{./img/gradient-descent.pdf_tex}
\end{center}

It computes, from a starting point $M_0$ that belongs to $f(x)$ (the
function to find the minimum), a next point by \textit{descending} the
tangent. The tangent at $M_0$ is given by the first order derivative
of the function $f'(x_0)$. This process is repeated until the
algorithm converge to a single point that means that the algorithm find a
\textit{local minima}. In a similar way of the Perceptron, the
convergence rate can be adjusted with a coefficient noted $\eta$. The
algorithm is quite simple and can be described with the following
recursive formula:
\begin{equation}
  x_{k+1} = x_k - \eta f'(x_k)
\end{equation}
where $k$ is the step number of the computation and $x_k$ is the X
axis value at the $k$th step of the algorithm.

\section*{Let's code a 2D version}
The objective is to code the gradient descent algorithm for the following function:
\begin{equation}
  f(x) = x^4 - 3x^3 + 2 \nonumber
\end{equation}


\begin{task}
  considering the starting point $ x_{0} = 0.2$, a rate
  $\eta = 0.01$, and a first derivative $f'(x) = 4x^3 - 9x^2$, implement the gradient descent algorithm to deduce
  the minima of $f(x)$. The criterion for stopping the gradient descent algorithm is :
  \begin{equation}
  \lvert x_{k+1} - x_{k} \rvert < 0.00001 \nonumber
\end{equation}
You must find the local minima at $x=2.25$.
\end{task}

\begin{info}
  The correction of this part is given by \dltutodnn{05}{tuto-05\_correction-part1.py}
\end{info}


The figure \ref{fig:grad-2D} shows the converging rate of the related gradient
descent algorithm. On this plot, the orange points highlight the
different steps of the algorithm. It suggests that the points roll
down on the curve!  In fact, the gradient descent algorithm allows to
move the next point in a given \textit{direction} and an
\textit{intensity}. In 2D, only two directions are possible: move to
the left or move to the right whereas the intensity is related to the
step between two consecutive points. You can note that this step is
not constant (in contrast of the original Perceptron algorithm) and it
decreases while approaching the local minima. It means that the
recursive algorithm works well and converges toward the right minima
value.

\begin{figure}[h]
\begin{center}
  \svg{.6\linewidth}{./img/gradient-descent1.pdf_tex}
  \caption{2D gradient descent algorithm in action for $f(x)=x^4 - 3x^3 + 2$\label{fig:grad-2D}}
\end{center}
\end{figure}

\begin{task}
  Now, consider the starting point at $x_{0} = −0.5$. What happens ? 
\end{task}

\begin{info}
In fact, the algorithm is trapped in a local minima
that do not correspond to the global minima of the function.
\end{info}

\begin{task}
  Now, consider the initial starting point at $ x_{0} = 0.2$ and a higher rate value $\eta = 0.1$. What happens ? 
\end{task}

\begin{info}
The algorithm may diverge if $\eta$ is too high. So, small $\eta$
values ensure convergence but the number of iteration to compute
becomes high and the algorithm slows down.
\end{info}

\section*{Let's code a 3D version!}

The gradient descent algorithm can also be applied in a 3 dimensional space.
Imagine that the function $f$ depends on two variables $x$
and $y$. So, $f(x,y)$ can be plotted as a surface in a three dimensional
space. In this 3 dimensional space, the gradient descent algorithm becomes:
\begin{equation}
  (x_{k+1},\ y_{k+1}) = (x_k,\ y_k) - \eta \nabla f(x_k,\ y_k)\nonumber 
\end{equation}
where $\nabla$ is the gradient operator:
\begin{equation}
  \nabla f(x,\ y) = \left( \frac{\partial f}{\partial x}  \ \  \frac{\partial f}{\partial y} \right)\nonumber 
\end{equation}

Let's try it on a practical example. Suppose the following function:
\begin{align}
  f(x,y) &= \sin\left(0.1\ x^2 + 0.05\ y^2 + y\right)\nonumber
\end{align}
Here, the partial derivatives required for computing the gradient  $\nabla f(x,\ y)$ are:
\begin{align}
  \frac{\partial f}{\partial x} (x,y)&= \cos\left(0.1\ x^2 + 0.05\ y^2 + y\right) (0.2\ x)\nonumber \\
  \frac{\partial f}{\partial y} (x,y)&= \cos\left(0.1\ x^2 + 0.05\ y^2 + y\right) (0.1\ y + 1)\nonumber
\end{align}
From these above formula, the gradient descent algorithm can be
implemented.

\begin{task}
  Complete the \dltutodnn{05}{tuto-05\_part2.py} script file for deducing the $x,y$ values of the global minima of the function $f(x,y)$
\end{task}

\begin{info}
  The correction of this part is given by \dltutodnn{05}{tuto-05\_correction-part2.py}
\end{info}

The figure \ref{fig:grad-3D} shows the gradient descent
algorithm in action from the starting point $(-4,\ -0.5)$ for the function $f(x,y)$.
\begin{figure}[h]
  \begin{center}
    \svg{.6\linewidth}{./img/gradient-descent-3D.pdf_tex}
    \caption{3D gradient descent algorithm in action for $f(x,y)=\sin\left(0.1\ x^2 + 0.05\ y^2 + y\right)$\label{fig:grad-3D}}
  \end{center}
\end{figure}

\section*{N-spaces generalization}

In fact, the gradient descent algorithm can be generalized in $n$-dimensional
spaces. In $n$-dimensional spaces, the function $f$ can be a function of $n$
independent variables as:
\begin{equation}
  f(x_0,x_1,\dots,x_n) = f(\mathbf{x})\quad \text{where}\quad \mathbf{x}(x_0,x_1,\dots,x_n)\nonumber 
\end{equation}
the gradient descent algorithm can be simply written in a vectorial form as:
\begin{equation}
  \mathbf{x}_{k+1} = \mathbf{x}_k - \eta \nabla f(\mathbf{x}_k)\nonumber 
\end{equation}
where $\nabla f(\mathbf{x})$ is the gradient vector of $f$ defined as:
\begin{equation}
  \nabla f(\mathbf{x}) = \left( \frac{\partial f}{\partial x_0} , \ \frac{\partial f}{\partial x_1}, \dots ,  \frac{\partial f}{\partial x_n}\right)\nonumber 
\end{equation}

To summarize, at this point, we have an algorithm able to find the minimum of a
continuum function in a $n$ dimensional spaces. The gradient descent
algorithm is extensively used in neural networks for minimizing loss
functions. The next tutorial will describe this process.


\end{document}
