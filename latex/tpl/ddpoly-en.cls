\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ddpoly-en}[5/9/13 dd custom polycop]
\LoadClassWithOptions{article}


% 'eleve' is an option

% use landscape option to print slides
% for example \documentclass[a4paper,french,trou,landscape]{tpl/ddpoly}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}

\newcommand{\cours}{\def\ddactivity{\textbf{\textsc{Courses}}}}
\newcommand{\td}{\def\ddactivity{\textbf{\textsc{Tutorial}}}}
\newcommand{\tp}{\def\ddactivity{\textbf{\textsc{Practical work}}}}
\newcommand{\projet}{\def\ddactivity{\textbf{\textsc{Project}}}}
\newcommand{\ds}{\def\ddactivity{\textbf{\textsc{Evaluation}}}}
\newcommand{\note}{\def\ddactivity{Notes}}
\newcommand{\rien}{\def\ddactivity{}}
\newcommand{\activity}[1]{\def\ddactivity{#1}}

\newcommand{\ddmyversion}{\textit{\today\ version}, \printddauthor}
\newcommand{\ddmypage}{page \thepage{} on \pageref{LastPage}}
\newcommand{\mylicence}{ \textit{This document is placed under the creative commons BY-SA} license\\ 
  \includegraphics[height=.5cm]{by-sa}}




\input{tpl/ddpolycommon}


