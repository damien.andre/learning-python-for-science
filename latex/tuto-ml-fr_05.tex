\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\activity{\textbf{Crédit} : \href{https://github.com/cjlux/APP-ML/blob/master/APP-ML\_2023/Notebooks}{JL. Charles} (CC BY-SA 4.0)}




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newmlexo{05}{Exploiter un réseau de neurones}

\noindent L'objectif de ce tuto est d'exploiter le réseau de neurones
que vous avez entraîné et sauvegardé au tuto précédant. Vous allez
donc maintenant vous servir de votre réseau pour faire des \textbf{prédictions}.

\begin{center}
  \includegraphics[width=\linewidth]{archi-dnn-mnist}
\end{center}


\begin{manip}
  Avant de démarrer le tuto, réalisez les opérations suivantes :
  \begin{enumerate}
  \item créez un environnement Python
  \item téléchargez le script \dltutoml{05}{part-0.py} et placez-le dans votre répertoire de travail 
  \item téléchargez le script \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/ml/mltool.py}{\texttt{mltool.py}} et placez-le dans votre répertoire de travail
  \item vérifier que votre réseau de neurones est sauvegardé dans votre répertoire de travail
  \end{enumerate}
\end{manip}

\begin{info}
  Le fichier \dltutoml{04}{part-0.py} vous servira de script de départ
\end{info}

\subsection*{Partie 1 : découverte de la fonction \keyw{predict(...)} sur une image}

\begin{task}
  À l'aide du
  \href{https://gitlab.com/damien.andre/learning-python-for-science/-/raw/master/latex/tuto-ml-fr_02.pdf?inline=false}{tuto
    \#2}, affichez la 50ème image du jeu de données de test.
\end{task}

\begin{task}
  À l'aide de la fonction
  \href{https://www.tensorflow.org/api_docs/python/tf/keras/Model#predict}{\texttt{predict(...)}}
  de la classe \keyw{model}, vérifiez que le réseau de neurones prédit la bonne valeur pour cette 50ème image.
\end{task}

\begin{info}
La correction de cette partie est donnée par \dltutoml{05}{part-1.py}
\end{info}

\subsection*{Partie 2 : qualifier la justesse des prédictions}

\begin{info}
  Nous allons tenter de qualifier et de quantifier la justesse des prédictions
  réalisées par votre réseau de neurones. Pour ce faire, nous allons
  utiliser l'ensemble des données de test et non pas une seule image
  comme dans la partie précédante.
\end{info}

\begin{task}
  À l'aide de la fonction
  \href{https://www.tensorflow.org/api_docs/python/tf/keras/Model#predict}{\texttt{predict(...)}}
  de la classe \keyw{model}, mettez en oeuvre le réseau de neurones
  sur l'ensemble des 10000 images de test.
\end{task}

\begin{task}
  Comparez le résultat des prédictions avec les données "labelisées"
  (tableau \keyw{lab\_test}) de façon à compter le nombre total de prédictions
  correctes.
\end{task}

\begin{task}
  Affichez, en pourcentage le nombre de prédictions correctes de votre réseau de neurones.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutoml{05}{part-2.py}
\end{info}

\subsection*{Partie 3 : matrice de confusion}

\begin{info}
  La partie précédante nous a permis de quantifier la qualité globale
  du réseau de neurones. C'est une information intéressante, mais
  quelque peu insuffisante. En effet, dans la perspective d'améliorer
  le réseau de neurones, nous devons mieux connaître les points forts
  et les points faibles du réseau de neurones. C'est ici qu'intervient
  la \href{https://fr.wikipedia.org/wiki/Matrice_de_confusion}{matrice de confusion}.
\end{info}

\begin{manip}
  Ouvrez le fichier \keyw{mltool.py} et observez la fonction \keyw{compute\_confusion\_matrix(...)}
\end{manip}

\begin{task}
  À l'aide de cette fonction, calculez puis affichez la matrice de confusion.
\end{task}

\begin{task}
  Identifiez alors le chiffre le plus difficile à reconnaître pour
  votre réseau de neurones.
\end{task}

\begin{task}
  Affichez le nombre total d'images pour chacun des
  chiffres. Autrement dit, vous devez compter le nombre d'images de
  \texttt{'0'}, de \texttt{'1'}, etc. Que remarquez-vous ? Les données
  affichées par la matrice de confusion sont-elles pertinentes pour
  identifier les difficultés de votre réseau de neurones ?
\end{task}

\begin{task}
  Implémentez une nouvelle version de la matrice de confusion qui
  affiche des pourcentages et non pas des valeurs absolues. Identifiez
  alors le chiffre le plus difficile à reconnaître pour votre réseau
  de neurones.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutoml{05}{part-3.py}
\end{info}


% \subsection*{Partie 4 : défi ML!}

% \begin{info}
% Les taches proposées ci-dessous sont par ordre de difficulté !
% \end{info}

% Vous allez maintenant utiliser votre réseau de neurones sur vos
% propres images. Tout d'abord, vous allez tester le numéro qui semble
% le plus facile à reconnaitre par votre réseau de neurones.

% \begin{task}
%   Dessinez un numéro sur une feuille blanche A4 puis
%   photographiez-le. Réalisez les traitements numériques nécessaires
%   pour adapter la photo au réseau de neurones puis testez votre image dans
%   le réseau de neurones et vérifiez sa prédiction.
% \end{task}

% L'objectif de la tache précédante est de "prototyper" un algorithme
% qui vous permettra d'automatiser un maximum de tache entre votre photo
% brute et le réseau de neurones.

% \begin{task}
%   Réalisez trois dessins (en alternant l'auteur des dessins) de chaque
%   des numéros. Vous devez donc, au total, avoir 30 dessins. Testez
%   votre réseau de neurones avec ces 30 dessins.
% \end{task}

% \begin{attention}
%   Maintenant, on passe en mode \textbf{SUPER DÉFI !}
% \end{attention}

% \begin{task}
%   À l'aide d'opencv, réalisez des reconnaissances de chiffres
%   sur une vidéo (déjà enregistrée) puis sur un flux vidéo "live" ! 
% \end{task}

\end{document}
