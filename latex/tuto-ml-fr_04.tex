\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\activity{\textbf{Crédit} : \href{https://github.com/cjlux/APP-ML/blob/master/APP-ML\_2023/Notebooks}{JL. Charles} (CC BY-SA 4.0)}




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newmlexo{04}{entraîner son réseau de neurones}

\noindent L'objectif de ce tuto est de mettre en oeuvre un réseau de
neurones dense sur les données de la banque \textsc{mnist} que vous
avez préalablement préparées. Attention, ce tuto n'est qu'un
\textbf{infime aperçu} des réseaux de neurones ! 

\begin{center}
  \includegraphics[width=\linewidth]{archi-dnn-mnist}
\end{center}


\begin{manip}
  Avant de démarrer le tuto, réalisez les opérations suivantes :
  \begin{enumerate}
  \item créez un environnement Python
  \item téléchargez le script \dltutoml{04}{part-0.py} et placez-le dans votre répertoire de travail 
  \item téléchargez le script \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/ml/mltool.py}{\texttt{mltool.py}} et placez-le dans votre répertoire de travail 
  \end{enumerate}
\end{manip}

\begin{info}
  Le fichier \dltutoml{04}{part-0.py} vous servira de script de départ
\end{info}

\subsection*{Partie 1 : création du réseau de neurones}

\begin{attention}
  Nous allons utiliser le module \texttt{keras} et \texttt{tensorflow}
  pour construire le réseau de neurones. Dans ce tuto, je vous livre
  "clé en main" une architecture de réseau neurones adaptée à ce
  problème. Il ne s'agit \textbf{donc pas de comprendre en détail le
    fonctionnement d'un réseau de neurones}
\end{attention}

Voici le réseau de neurones dense sur lequel nous allons travailler :
\vspace*{-1.5\baselineskip}
\begin{lstlisting}
model = keras.Sequential()
model.add(keras.Input(shape=(784,)))
model.add(layers.Dense(784, activation="relu"))
model.add(layers.Dense(10, activation="softmax"))
model.compile(loss='categorical_crossentropy', optimizer="adam", metrics=["accuracy"])
\end{lstlisting}

\begin{task}
  Essayez d'identifier les similarités entre le code ci-dessus et l'image du réseau de neurones.
\end{task}

\begin{task}
  Ajoutez, à la suite du fichier \dltutoml{04}{part-0.py}, le code
  ci-dessus pour créer le réseau de neurones. 
\end{task}

\begin{info}
  À ce stade, le réseau n'est pas entraîné (les poids des neurones sont complètement aléatoires).
\end{info}

\subsection*{Partie 2 : entraînement du réseau de neurones}

Pour entraîner le réseau de neurones, nous allons utiliser le code suivant :
\vspace*{-1.5\baselineskip}
\begin{lstlisting}
hist = model.fit(x_train, y_train, batch_size=128, epochs=15)
\end{lstlisting}
\begin{info}
Dans le code ci-dessus :
\begin{enumerate}
\item \texttt{hist} contient l'historique de
  l'apprentissage
\item \texttt{epochs} est le
  nombre de fois ou l'apprentissage complet est rejoué
\item \texttt{batch\_size} est le nombre d'images traité simultanément lors de l'apprentissage
\end{enumerate}
\end{info}

\begin{task}
   Ajoutez à votre script le code ci-dessus. Lancez votre script. Observez le résultat.
\end{task}

\begin{info}
Nous allons maintenant observer l'historique de votre apprentissage
grâce à la fonction \keyw{plot\_loss\_accuracy(...)} du module
\keyw{mltool} que vous avez téléchargé.
\end{info}

\begin{task}
  Ajoutez à votre script le code suivant puis lancez votre script et observez le résultat.
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
mltool.plot_loss_accuracy(hist)
\end{lstlisting}
\end{task}

\begin{info}
  Vous devriez alors visualiser les graphes suivants
  \dltutoml{04}{part-2\_hist.svg} qui montrent l'évolution de
  \textbf{la justesse} des prédictions (accuracy) et l'évolution de
  \textbf{l'erreur} (loss) en fonction des epochs.
\end{info}

\begin{task}
  Observez ces deux courbes. Vers quelles valeurs tendent-elles ? Est-ce normal ?
\end{task}

\begin{info}
La correction de cette partie est donnée par \dltutoml{04}{part-2.py}
\end{info}

\subsection*{Partie 3 : prise en compte des données de test}
\begin{info}
  Pour l'instant, nous n'avons pas pris en compte les données de
  test. Nous ne savons donc pas si le réseau de neurones a de bonnes
  capacités de prédiction sur le jeu de test.
\end{info}

\begin{task}
  En vous inspirant de la partie précédante, entraînez de nouveau
  votre réseau de neurones avec la fonction
  \href{https://www.tensorflow.org/api_docs/python/tf/keras/Model#fit}{\texttt{fit(...)}}. Intégrez
  cette fois dans la fonction
  \href{https://www.tensorflow.org/api_docs/python/tf/keras/Model#fit}{\texttt{fit(...)}}
  les données de test \keyw{x\_test}, \keyw{y\_test} grâce à l'argument \keyw{validation\_data}.
\end{task}

\begin{task}
  Tracez l'évolution de la justesse et de l'erreur. Vous devriez
  obtenir \dltutoml{04}{part-3\_hist.svg}. Observez ces deux
  courbes. Que remarquez-vous ? Est-ce optimal de faire 15 epochs ?
\end{task}

\begin{info}
  Vous venez de découvrir l'over-fitting. C'est lorsqu’ un réseau
  de neurones est surentrainé !
\end{info}

\begin{info}
La correction de cette partie est donnée par \dltutoml{04}{part-3.py}
\end{info}

\subsection*{Partie 4 : sauvegarder son réseau de neurones}
\begin{info}
  L'entraînement de votre réseau de neurones n'a duré que quelques
  secondes. Dans les cas réels, un entraînement peut prendre plusieurs
  heures, voir plusieurs jours. Il est donc important de pouvoir
  sauvegarder l'état d'un réseau de neurones. Il sera possible
  de le re-charger ultérieurement et permettre ainsi son utilisation
  différée.
\end{info}

\begin{task}
  À l'aide de la documentation
  \href{https://www.tensorflow.org/guide/keras/save_and_serialize}{Enregistrer
    et charger les modèles Keras}, sauvegardez
  votre réseau de neurones \keyw{model} dans un dossier que vous nommerez
  \keyw{DNN} (pour Dense Neural Network).
\end{task}

\begin{task}
  À l'aide de la documentation
  \href{https://www.tensorflow.org/guide/keras/save_and_serialize}{Enregistrer
    et charger les modèles Keras}, chargez 
  votre réseau de neurones \keyw{DNN} dans une variable \keyw{model1}.
\end{task}

\begin{task}
  Vérifiez à l'aide de la fonction
  \href{https://keras.io/api/models/model/#summary-method}{\texttt{summary(...)}}
  que \keyw{model} et \keyw{model1} ont bien la même architecture. 
\end{task}

\begin{info}
La correction de cette partie est donnée par \dltutoml{04}{part-4.py}
\end{info}

\subsection*{Partie 4 : défi ML!}

\begin{task}
  Ajustez les paramètres du réseau de neurones pour que l'erreur du
  jeu de test soit inférieure à 5 \%. Vous pouvez ajuster de nombreux
  paramètres, notamment, vous pouvez jouer avec l'architecture du
  réseau de neurones (indiquée en partie 1 de ce tuto).
\end{task}

\begin{task}
  Sauvegardez votre réseau de neurones optimisé pour une utilisation ultérieure.
\end{task}


\end{document}
