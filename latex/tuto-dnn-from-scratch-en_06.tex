\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}
\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newdnnexo{06}{The loss function}

\noindent Loss functions are special functions able to measure the
accordance of a prediction with their target values. High scores
indicate bad predictions whereas low scores indicate good accordance
of predictions. The Perceptron architecture with loss function $L$ is
given by the following flow chart.
\begin{center}
  \svg{.7\linewidth}{./img/perceptron-scheme-simple.pdf_tex}
\end{center}



A common loss function (noted $L$) often used in machine
learning is the Mean Square Error (MSE). The MSE function $L$ is defined as:
\begin{equation}
  L(\mathbf{y}) = \frac{1}{m} \sum\limits_{i=1}^{m} \left( y^{(i)} - t^{(i)} \right)^2\nonumber
\end{equation}
where $m$ is the total number of data points, $y^{(i)}$ and $t^{(i)}$
are related to a unique instance of a prediction and its target
value. The idea of the back propagation step is to minimize the loss
function $L(\mathbf{y})$ in regard of weights $\mathbf{w}$. For sure,
the gradient descent algorithm can be used to make this job. So, the
gradient of the loss function in regard of weights have to be
computed.
\begin{equation}
  \nabla L(\mathbf{w}) = \left( \frac{\partial L}{\partial w_0} , \ \frac{\partial L}{\partial w_1}, \dots ,  \frac{\partial L}{\partial w_n}\right)\nonumber 
\end{equation}
If we focus on the first component of the gradient $\nabla L(\mathbf{w})$, it gives:
\begin{align}
  \frac{\partial L}{\partial w_0}
  &= \frac{\partial}{\partial w_0}\left( \frac{1}{m} \sum\limits_{i=1}^{m} \left( y^{(i)} - t^{(i)} \right)^2 \right)\nonumber
 \end{align}
By the way, the derivative can be introduced in the sum as:
\begin{align}
  \frac{\partial L}{\partial w_0}
  &=  \frac{1}{m} \sum\limits_{i=1}^{m} \frac{\partial}{\partial w_0}\left( \left( y^{(i)} - t^{(i)} \right)^2\right)\nonumber
\end{align}
Following the Perceptron architecture, one notes that the output $y$
is a composed function of $z$ and $a$ as:
\begin{align}
  y &=y\left(\ z\ \right) \nonumber\\
    &=y\left(\ z(\ a\ )\ \right)\nonumber\\
    &=y\left(\ z\left(\ a(\mathbf{x}, \mathbf{w})\  \right) \ \right)\nonumber
\end{align}
So, the following derivative chain rule can be written:
\begin{align}
  \frac{\partial f}{\partial w_0} =
  \frac{\partial  f}{\partial  y}\cdot 
  \frac{\partial  y}{\partial  z}\cdot 
  \frac{\partial  z}{\partial  a}\cdot  
  \frac{\partial  a}{\partial  w_0} \quad \text{where} \quad
  f(y) = \left( y - t \right)^2\nonumber
\end{align}
The derivative of each chain item can be easily computed:
\begin{align}
  \frac{\partial  f}{\partial  y}   &= \frac{\partial}{\partial y}\left( \left( y - t \right)^2 \right) &=& 2(y-t)                &\rightarrow\quad& \text{\textit{derivative of loss function (MSE)}}\nonumber\\
  \frac{\partial  y}{\partial  z}   &= \frac{\partial}{\partial  z}\left(z  \right) &=& 1                                         &\rightarrow\quad& \text{\textit{derivative of output function (identity)}}\nonumber\\
  \frac{\partial  z}{\partial  a}   &= \frac{\partial}{\partial  a}\left(\sigma(a)\right) =\sigma(a)(1-\sigma(a)) &=& \sigma '(a)              &\rightarrow\quad& \text{\textit{derivative of activation function (sigmoid)}}\nonumber\\
  \frac{\partial  a}{\partial  w_0} &= \frac{\partial}{\partial  w_0}\left(b + x_0 w_0 + x_1 w_1 + \dots +  x_n w_n  \right) &=& x_0 &\rightarrow\quad& \text{\textit{derivative of transfer function}}\nonumber
\end{align}
We can combine all these derivatives in one single formula. It gives:
\begin{align}
  \frac{\partial L}{\partial w_0}
  &=  \frac{1}{m} \sum\limits_{i=1}^{m} 2(y^{(i)} - t^{(i)})\cdot 1\cdot \sigma '(a^{(i)})\cdot x_{0}^{(i)} \nonumber
\end{align}
This formula can be generalized to any weight $w_j$ as:
\begin{equation}
  \boxed{\frac{\partial L}{\partial w_j} =  \frac{2}{m} \sum\limits_{i=1}^{m} (y^{(i)} - t^{(i)})\cdot \sigma '(a^{(i)})\cdot x_{j}^{(i)}} \label{eq:1}
\end{equation}
and for the bias $b$:
\begin{equation}
  \boxed{\frac{\partial L}{\partial b} =  \frac{2}{m} \sum\limits_{i=1}^{m} (y^{(i)} - t^{(i)})\cdot \sigma '(a^{(i)})} \label{eq:2}
\end{equation}

\begin{info}
  Here, the trick for computing quickly the derivative of $L$
  regarding the bias $b$ is to consider $b$ as a weight where its
  entry is always equal to 1.  Practically,
  in some machine learning frameworks, this feature is given by adding
  silently a new component to the $\mathbf{x}$ vector (which is always
  equal to a unit) and also to the weight $\mathbf{w}$ (which is equal
  to the bias). It gives:
  \begin{equation}
    a =
    \begin{bmatrix}
    1 \\x_0  \\ x_1  \\ \vdots \\ x_n
\end{bmatrix} \cdot \begin{bmatrix}
  b \\ w_0  \\ w_1  \\ \vdots \\ w_n
\end{bmatrix} = b + x_0 w_0 + x_1 w_1 + \dots + x_n w_n\nonumber
\end{equation}
So, it is equivalent to the classical transfer function. It allows to
remove the bias for avoiding its special treatment. This trick is
often used in the literature about neural network! For pedagogical
reason this trick will not be used in this document.
\end{info}

\begin{attention}
Don't be confuse, remember that $n$ is related to the dimension of the
entry vector $\mathbf{x}(x_0, x_1, \cdots , x_n$) whereas $m$ is
related to the number of entry points. In other words, the learning
step involves $m$ points of $n$-dimensions.
\end{attention}

\section*{Let's code!}

\begin{manip}
  Thanks to the first tutorial, create a Python environment \texttt{(dnn)} and launch the \textbf{thonny} editor.
\end{manip}

\begin{manip}
  Download the
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/dnn/mltool.py}{\texttt{mltool.py}}
   file and put it in your current working dir.
\end{manip}

\begin{manip}
  Download the \dltutodnn{06}{tuto-06.py} script file and
  execute it with \textbf{thonny}. This file will be used as starting point for
  this tutorial. \textbf{Note that this file is the last step of the \href{https://gitlab.com/damien.andre/learning-python-for-science/-/raw/master/latex/tuto-dnn-from-scratch-en_04.pdf?inline=false}{tutorial \#4}}.
\end{manip}

\begin{task}
  Edit this script file for implementing the Perceptron with gradient
  descent and MSE loss function. Choose a
  \textbf{non-vectorized version} of the gradient descent
  algorithm. In such a case, you can consider $m=1$ that leads to
  avoid summation in equation \ref{eq:1} and \ref{eq:2}.
\end{task}

\begin{task}
  Store the total cumulative loss per epoch in an array to monitor the
  evolution of error versus epoch. Plot this array. You must obtain the graph below.
  \begin{center}
  \svg{.5\linewidth}{./img/loss_1.pdf_tex}
\end{center}
\end{task}


\begin{info}
  The correction of this part is given by \dltutodnn{06}{tuto-06\_correction-part1.py}
\end{info}

\section*{Conclusion}
In the next tutorial step, you will learn how to implement a very fast
and accurate version of this algorithm using a vectorization technique.


\end{document}
