\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}

\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Vision par ordinateur}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newvisionexo{04}{reconnaissance de grain (partie 2)}

\noindent L'image suivante est une image réelle de Microscope
Électronique à Balayage (MEB) qui montre une poudre de
\href{https://heegermaterials.com/spherical-powder/1236-spherical-titanium-powder.html}{Titane}
(Ti).  L'objectif de cet exercice est d'extraire de cette image
quelques données statistiques sur cette poudre : répartition
granulométrique et sphéricité.

\begin{center}
  \includegraphics[width=0.6\linewidth]{../script/vision/tuto-04/grain.png}
\end{center}

\begin{attention}
Attention, cet exercice est difficile, car vous êtes en "situation
réelle". Concentrez-vous surtout sur les parties 1 et 2. Les parties 3 et
4 sont optionnelles et ne sont à faire que si vous souhaitez aller plus loin. 
\end{attention}


\subsection*{Partie 1 : traitements préliminaires}

\begin{manip}
  Téléchargez l'image \dltutovision{04}{grain.png} et placez-la dans votre répertoire courant 
\end{manip}

\begin{task}
  En vous basant sur l'exercice précédant, réalisez les traitements suivants :
  \begin{enumerate}
  \item chargez l'image avec \keyw{imread(...)}
  \item convertissez cette image en niveau de gris avec \keyw{rgb2gray(...)}
  \end{enumerate}
\end{task}


\begin{task}
  À l'aide de l'échelle en bas à droite, relevez le nombre de pixels correspondant à une longueur de 200$\mu m$.
\end{task}

\begin{task}
  Enlever le bandeau d'information au bas de l'image. Vous devriez alors obtenir la figure
  suivante \dltutovision{04}{grain\_part1-1.png}.
\end{task}

\begin{task}
  Réalisez une copie de l'image ainsi obtenue à l'aide de la fonction \keyw{np.copy(...)}.
\end{task}

\begin{info}
  "Mettez au chaud" la copie issue de cette question, elle sera utilisée dans les parties suivantes !
\end{info}

\begin{task}
  Appliquez un seuillage de façon à obtenir l'image suivante \dltutovision{04}{grain\_part1-2.png}.
\end{task}

\begin{manip}
  Importez le module
  \href{https://docs.scipy.org/doc/scipy/reference/ndimage.html}{\texttt{ndimage}}
  de la bibliothèque
  \href{https://docs.scipy.org/doc/scipy/}{\texttt{scipy}} :
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
from scipy import ndimage
\end{lstlisting}
\end{manip}

\begin{task}
  À l'aide de la fonction
  \href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.binary_fill_holes.html}{\texttt{ndimage.binary\_fill\_holes(...)}},
  tentez d'obtenir l'image suivante
  \dltutovision{04}{grain\_part1-3.png}.  Que fait cette fonction ?
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{04}{grain\_part1.py}
\end{info}


\subsection*{Partie 2 : traitement naïf à l'aide de \keyw{ndimage.binary\_erosion(...)}}
\begin{info}
  L'objectif est maintenant d'obtenir une image ayant les caractéristiques suivantes :
  \begin{enumerate}
  \item les grains doivent être "remplis" au mieux
  \item les grains ne doivent pas se toucher 
  \end{enumerate}
  Ces deux caractéristiques permettront de faciliter le traitement
  grain par grain obtenu grâce à la fonction
  \href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.label.html}{\texttt{ndimage.label(...)}}
  que vous avez utilisée au tuto précédant.
\end{info}


\begin{task}
  À partir de l'image obtenue à la partie précédante et à l'aide de la
  fonction
  \href{https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.binary_erosion.html}{\texttt{ndimage.binary\_erosion(...)}},
  tentez d'obtenir l'image suivante
  \dltutovision{04}{grain\_part2-1.png}. Que fait cette fonction ?
  Quel paramètre permet de "séparer" plus ou moins les grains ?
\end{task}

\begin{task}
  En vous aidant de la fonction \keyw{ndimage.label(...)} (voir
  exercice précédant), dessinez le centre de chaque grain ainsi que
  leurs boites englobantes sur l'image que vous avez "réservé" à la
  \textit{task4}. Vous devriez obtenir l'image suivante :
  \dltutovision{04}{grain\_part2-2.png}.
\end{task}

\begin{task}
  Calculez le rayon moyen et convertissez-le en $\mu m$
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{04}{grain\_part2.py}
\end{info}

\subsection*{Partie 3 (optionnel) : traitement avancé à l'aide d'un masque}
  Le traitement numérique précédant n'est pas très satisfaisant. En
  effet, l'érosion entraîne la disparation des petits grains tout en
  altérant les diamètres de tous les grains. Nous allons essayer de
  trouver ici une solution plus sophistiquée. Pour cela, nous allons
  chercher à représenter les contours des grains.

\begin{info}
  Afin de trouver les contours des grains, nous allons exploiter le
  contraste de l'image. En effet, lorsqu'on passe brusquement d'un
  pixel clair à un pixel sombre cela signifie que l'on atteint le
  contour d'un grain. Or, les différences de niveau de gris de pixels
  contigus sont données à l'aide de la fonction
  \href{https://numpy.org/doc/stable/reference/generated/numpy.gradient.html}{\texttt{np.gradient(...)}}. Nous
  allons donc exploiter cette dernière fonction.
\end{info}

\begin{manip}
Testez le script suivant (où \keyw{img} est l'image obtenue à la \textit{task4})
  \vspace*{-1.5\baselineskip}
\begin{lstlisting}
grad_x, grad_y = np.abs(np.gradient(img))
img_grad = (grad_x + grad_y)/2
plt.imshow(img_grad, cmap='gray', vmin=0., vmax=1.)
plt.show()
\end{lstlisting}
  Vous devriez alors obtenir l'image suivante \dltutovision{04}{grain\_part3-1.png}
\end{manip}

\begin{task}
  Appliquez un seuillage de façon à obtenir l'image suivante \dltutovision{04}{grain\_part3-2.png}.
\end{task}

\begin{task}
  Inversez le noir et le blanc de façon à obtenir l'image suivante \dltutovision{04}{grain\_part3-3.png}.
\end{task}

\begin{info}
  Nous allons nous servir de cette image comme un masque pour dessiner
  les contours des grains sur l'image obtenue à la fin de partie 1.
\end{info}

\begin{task}
  Reprenez l'image obtenue à la fin de la partie 1. Nous appellerons
  cette image \texttt{img1}. Appliquez le masque. Le masque consiste à
  mettre en noir les pixels de \texttt{img1} si le pixel correspondant
  de \texttt{img\_grad} est également noir. Vous devriez obtenir
  l'image suivante \dltutovision{04}{grain\_part3-4.png}.
\end{task}

\begin{task}
  Utilisez de nouveau la fonction
  \keyw{ndimage.binary\_fill\_holes(...)}  de façon à obtenir l'image
  suivante \dltutovision{04}{grain\_part3-5.png}.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{04}{grain\_part3.py}
\end{info}

\subsection*{Partie 4 : traitement statistique sur l'image obtenue à la partie 3}
La partie précédante nous a permis d'avoir une image sur lequel
l'algorithme \keyw{ndimage.label(...)} sera efficace. En effet, les
grains sont bien séparés des uns des autres sans que leurs diamètres
n'en soient trop affectés.

\begin{task}
  De façon similaire à la \textit{task8} (re)dessiner le centre de
  chaque grain ainsi que leurs boites englobantes à l'aide de la
  fonction \keyw{ndimage.label(...)} sur l'image obtenue à la partie
  précédante.
\end{task}


\begin{task}
  Imaginez un filtre pour écarter les détections erronées. Vous
  devriez obtenir l'image suivante :
  \dltutovision{04}{grain\_part4-1.png}.
\end{task}


\begin{task}
  Calculez le rayon moyen et convertissez-le en $\mu m$
\end{task}

\begin{task}
  Comparez les résultats obtenus avec ceux de la partie 2. Qu'en
  concluez-vous ?
\end{task}

\begin{task}
  Tracez un histogramme représentant la distribution des rayons des
  grains.  Vous devriez obtenir l'image suivante :
  \dltutovision{04}{grain\_part4-2.png}.
\end{task}

\begin{info}
  On définit la circularité comme le rapport de la plus grande longueur
  sur la plus petite longueur de la boite englobante. Un grain est
  alors considéré comme circulaire si ce rapport est proche de 1.
\end{info}

\begin{task}
  Tracez un histogramme représentant la distribution de la circularité
  des grains de façon à obtenir l'image suivante :
  \dltutovision{04}{grain\_part4-3.png}.
\end{task}

\begin{info}
  La correction de cette partie est donnée par \dltutovision{04}{grain\_part4.py}
\end{info}

\end{document}
