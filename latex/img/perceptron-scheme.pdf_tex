%% Creator: Inkscape inkscape 0.92.3, www.inkscape.org
%% PDF/EPS/PS + LaTeX output extension by Johan Engelen, 2010
%% Accompanies image file 'perceptron-scheme.pdf' (pdf, eps, ps)
%%
%% To include the image in your LaTeX document, write
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics{<filename>.pdf}
%% To scale the image, write
%%   \def\svgwidth{<desired width>}
%%   \input{<filename>.pdf_tex}
%%  instead of
%%   \includegraphics[width=<desired width>]{<filename>.pdf}
%%
%% Images with a different path to the parent latex file can
%% be accessed with the `import' package (which may need to be
%% installed) using
%%   \usepackage{import}
%% in the preamble, and then including the image with
%%   \import{<path to file>}{<filename>.pdf_tex}
%% Alternatively, one can specify
%%   \graphicspath{{<path to file>/}}
%% 
%% For more information, please see info/svg-inkscape on CTAN:
%%   http://tug.ctan.org/tex-archive/info/svg-inkscape
%%
\begingroup%
  \makeatletter%
  \providecommand\color[2][]{%
    \errmessage{(Inkscape) Color is used for the text in Inkscape, but the package 'color.sty' is not loaded}%
    \renewcommand\color[2][]{}%
  }%
  \providecommand\transparent[1]{%
    \errmessage{(Inkscape) Transparency is used (non-zero) for the text in Inkscape, but the package 'transparent.sty' is not loaded}%
    \renewcommand\transparent[1]{}%
  }%
  \providecommand\rotatebox[2]{#2}%
  \newcommand*\fsize{\dimexpr\f@size pt\relax}%
  \newcommand*\lineheight[1]{\fontsize{\fsize}{#1\fsize}\selectfont}%
  \ifx\svgwidth\undefined%
    \setlength{\unitlength}{599.24541804bp}%
    \ifx\svgscale\undefined%
      \relax%
    \else%
      \setlength{\unitlength}{\unitlength * \real{\svgscale}}%
    \fi%
  \else%
    \setlength{\unitlength}{\svgwidth}%
  \fi%
  \global\let\svgwidth\undefined%
  \global\let\svgscale\undefined%
  \makeatother%
  \begin{picture}(1,0.57735193)%
    \lineheight{1}%
    \setlength\tabcolsep{0pt}%
    \put(0,0){\includegraphics[width=\unitlength,page=1]{perceptron-scheme.pdf}}%
    \put(0.07594776,0.45139489){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25000012}\smash{\begin{tabular}[t]{c}$x_{0}$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=2]{perceptron-scheme.pdf}}%
    \put(0.07594776,0.28723455){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25000012}\smash{\begin{tabular}[t]{c}$x_{1}$\end{tabular}}}}%
    \put(0.07099729,0.19711799){\color[rgb]{0,0,0}\rotatebox{-90}{\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}...\end{tabular}}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=3]{perceptron-scheme.pdf}}%
    \put(0.07594776,0.08109659){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25000012}\smash{\begin{tabular}[t]{c}$x_{n}$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=4]{perceptron-scheme.pdf}}%
    \put(0.20475836,0.43673548){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}$w_{0}$\end{tabular}}}}%
    \put(0.20475836,0.30460264){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}$w_{1}$\end{tabular}}}}%
    \put(0.20475836,0.1136453){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}$w_{n}$\end{tabular}}}}%
    \put(0.78036229,0.28333501){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}\textbf{output $y=z(a)$}\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=5]{perceptron-scheme.pdf}}%
    \put(0.07253748,0.557066){\color[rgb]{0,0,0}\makebox(0,0)[t]{\lineheight{1.25}\smash{\begin{tabular}[t]{c}input $\mathbf{x}$\end{tabular}}}}%
    \put(0.326487,0.00555384){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}weights $\mathbf{w}$\end{tabular}}}}%
    \put(0.4856422,0.50183825){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}net input function $a(x_{i}, w_{i}, b)$\end{tabular}}}}%
    \put(0.6058546,0.05437009){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\lineheight{1.25}\smash{\begin{tabular}[t]{l}activation function $z(a)$\end{tabular}}}}%
    \put(0,0){\includegraphics[width=\unitlength,page=6]{perceptron-scheme.pdf}}%
    \put(-0.15625217,0.7530705){\color[rgb]{0,0,0}\makebox(0,0)[lt]{\begin{minipage}{0.072064\unitlength}\raggedleft \end{minipage}}}%
  \end{picture}%
\endgroup%
