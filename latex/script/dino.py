from matplotlib import pyplot as plt 
import numpy as np

x = [38, 56 , 59 , 64 , 74]
y = [41 ,63 , 70 , 72 , 84]

fit = np.polyfit(x,y,1)
fit_fn = np.poly1d(fit) 

plt.plot(x,y, 'bo', x, fit_fn(x), '--k')
plt.xlabel('Femur size (cm)')
plt.ylabel('Humerus size (cm)')
plt.show()
