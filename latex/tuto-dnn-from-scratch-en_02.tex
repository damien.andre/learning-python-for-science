\documentclass[a4paper, trou]{tpl/ddpoly-en}
\usepackage{dirtree}
\usepackage{wasysym}
\usepackage{mathtools}
\usepackage{enumitem}
\setlist{nolistsep}
\input{macro}
\input{tpl/listing-config}

  
\graphicspath{{tpl/}{img/}}
\ddmodulename{Informatique}
\ddmodulenumber{Machine learning}
\ddauthor{D. André}
\dduniv{Ensil-Ensci}
\ddimg{\begin{center}\includegraphics[width=6cm]{img/arduino-logo}\end{center}}
\rien




\begin{document}
\renewcommand{\labelitemi}{$\bullet$}
\lstset{language=granCpp,style=granCpp}  
\lstset{language=granPython,style=granPython}  

\newdnnexo{02}{The Rosenblatt's perceptron}

\noindent
In 1957, The Rosenblatt's perceptron was the first learning
machine. As shown on picture below, it was really a machine! 

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=.5\linewidth]{perceptron}
    \caption{Le Perceptron de Rosenblatt en 1957}
  \end{center}
\end{figure}





Here, we will see the most simple example of \textit{supervised
  training}. It means that the machine will be trained with input data
(noted $\mathbf{x}$) that has already been classified (noted
$\mathbf{t}$). For example, the next figure shows an example of
already-classified data. Here, for the sake of clarity, the data
$\mathbf{x}$ are in two dimensions. Each point of the data is defined
by 2D coordinates $(x_{0}, x_{1})$ and each point is associated with a class. The
class is defined by a label $t$ that takes the '1' or '0' value.

\begin{center}
  \svg{0.5\linewidth}{./img/perceptron-plot.pdf_tex}
\end{center}


The workflow of the supervised machine learning is simple:
\begin{enumerate}
\item training the machine with already-classified data and
\item make prediction with non-classified data.
\end{enumerate}


The goal of the training step is to deduce the best weight values from
already-classified data. In the original Perceptron, these weights was
generated through mechanical potentiometers and their values were automatically
adjusted with motors!



Figure \ref{fig:perceptron} describes the Rosenblatt's Perceptron
architecture. The output is generated from the input through two
functions: the \textit{transfer function} and the \textit{activation
  function}. The weights $w_{i}$ are the coefficients that must be
trained and adjusted during the learning step.

\begin{figure}[h]
  \begin{center}
    \svg{0.9\linewidth}{./img/perceptron-scheme2.pdf_tex}
  \end{center}
  \caption{Overview of the Perceptron architecture \label{fig:perceptron}}
\end{figure}



\section*{The feed forward rule}
The \textit{feed forward} rule is the rule that makes output value(s)
from input values. It induces different mathematical
processes and computations. In the Perceptron algorithm, two computations are
processed given by two different functions: the \textit{transfer}
function and the \textit{activation} function.


\subsection*{The transfer function \label{sec:transfer-perceptron}}
The \textit{transfer function} makes a linear combination of the
inputs $(x_0, x_1, \dots, x_n)$ with the weights
$(w_0, w_1, \dots, w_n)$ and a single coefficient $b$ named
\textit{bias}. The transfer function is:
\begin{align}
  a(x_i) &= b + x_0 w_0 + x_1 w_1 + \dots + x_n w_n  \nonumber \\
         &= b + \sum\limits_{i=1}^{n}( x_i w_i) \nonumber
\end{align}
We can simplify the mathematical written by expressing inputs
$\mathbf{x}$ and weights $\mathbf{w}$ with vectors:
\begin{align}
  \mathbf{x} = 
  \begin{bmatrix}
    x_{0}  \\
    x_{1}  \\
    \vdots \\
    x_{n} 
\end{bmatrix}
  \quad \text{and} \quad
  \mathbf{w} =
  \begin{bmatrix}
    w_{0}  \\
    w_{1}  \\
    \vdots \\
    w_{n} 
\end{bmatrix}\nonumber
\end{align}
Now, the transfer function $a$ can be written as:
\begin{equation}
  a( \mathbf{x}, \mathbf{w}, b)  = \mathbf{x} \cdot \mathbf{w} + b\label{eq:net-input-perceptron}
\end{equation}
where $\cdot$ is the scalar (or dot) product between two vectors.

Note that the transfer function gives a scalar $a$. This function is
\textbf{really important}. It allows us to make linear combination of
entries. This function is already used even in complex modern neural networks such
as convolutional neural networks used in deep learning.


\subsection*{The activation function \label{sec:activation-perceptron}}
The second function is called \textit{activation function}. This is
always a non-linear function. In our case (the original Perceptron),
the activation function is the output of the Perceptron and must
be able to separate the two classes with \texttt{'1'} nor \texttt{'0'} values. The
\textit{unit step function} can do this job. The unit step function is defined as:
\begin{equation}
  z(a) = \left\{
    \begin{array}{rl}
         1 & \mbox{if } a>0 \\
         0 & \mbox{otherwise}
    \end{array}
\right.\nonumber
\end{equation}
Graphically, the unit step function gives:
\begin{center}
  \svg{0.2\linewidth}{./img/unit-step.pdf_tex}
\end{center}



\section*{The backward propagation rule}
At this step, we are able to compute an output $y$ that takes \texttt{'1'} nor
\texttt{'0'} values from an input $\mathbf{x}$. However, the weights
$\mathbf{w}$ has not been trained and the computed output is
irrelevant. The backward propagation rule allows to deduce the best
fit weights from already-classified data.

The rule proposed by Rosenblatt is simple. For each input data
$\mathbf{x}$, the weight $\mathbf{w}$ and bias $b$ are adjusted with a
small correction $\mathbf{\Delta w}$ and
$\Delta b$ as follows:
\begin{align}
  \mathbf{w} &\rightarrow \mathbf{w} + \eta \mathbf{\Delta w}\\\nonumber
  b &\rightarrow b + \eta \Delta b\nonumber
\end{align}
where $\eta$ is a coefficient named the \textit{learning rate}. Note that $\eta$ is a scalar.
Rosenblatt has proposed to deduce these corrections from the following rules:
\begin{align}
  \Delta w_0 &= \eta\left(t-y \right)x_0 \nonumber\\ 
  \Delta w_1 &= \eta\left(t-y \right)x_1 \nonumber\\
  \vdots &\nonumber\\
  \Delta w_n &= \eta\left(t-y \right)x_n \nonumber\\ 
  \Delta b   &= \eta\left(t-y \right) \nonumber
\end{align}
where $t$ is the target value and $y$ is the output of the
Perceptron. The above formula can be written in vectorial form as:
\begin{align}
  \mathbf{\Delta w} &= \eta\left(t-y \right)\mathbf{x} \nonumber\\ 
  \Delta b          &= \eta\left(t-y \right) \nonumber
\end{align}
With this learning rule, if $t=y$ the weights are not modified. If
$t\neq y$ the weight values are adjusted. This trick allows to
increase or decrease the weights in the direction of the target
value. The rate of these modifications can be adjusted thanks to the $\eta$
coefficient.


\section*{Let's coding!}

\begin{manip}
  Thanks to the first tutorial, create a Python environment \texttt{(dnn)} and launch the \textbf{thonny} editor.
\end{manip}

\begin{manip}
  Download the
  \href{https://gitlab.com/damien.andre/learning-python-for-science/blob/master/script/dnn/mltool.py}{\texttt{mltool.py}} 
  file and put it in your current working dir.
\end{manip}

\begin{manip}
  Download the \dltutodnn{02}{tuto-02.py} script file and
  execute it with \textbf{thonny}. This file will be used as starting point for
  this tutorial.
\end{manip}

\begin{task}
  Thanks to the thonny's interactive python prompt, display the type,
  the shape and the content of the \keyw{data} and \keyw{label} variables.
\end{task}

You can see that \texttt{data} is a numpy array. The first axis
length (1000) corresponds to the number of points specified with the
\texttt{n\_samples} argument of the \texttt{make\_blobs(...)} function. The
second axis length corresponds to the (2D) coordinates of each
point that corresponds to the \texttt{n\_features} argument of the
\texttt{make\_blobs(...)} function. Here, we have chosen two dimensions for an
easy plotting of data. To summarize, \texttt{data} is simply an array that
contains 1,000 two dimensional data points.


In addition, these 1,000 data points are classified through the \texttt{label}
variable.
\vspace*{-1\baselineskip}
\begin{lstlisting}
>>> type(label)
<type 'numpy.ndarray'>
>>> label.shape
(1000,)
\end{lstlisting}
As you can see, the \texttt{label} variable contains the class (also
called label) of each 1,000 points. Here, it contains only two classes
identified by '0' or '1'. You can choose more classes with the
\texttt{centers} argument of the \texttt{make\_blobs(...)} function.

\begin{center}
  \includegraphics[width=.8\linewidth]{data-label}
\end{center}

The \texttt{make\_blobs(...)} function, that comes from the \texttt{sklearn}
module, generates data cloud (a blob) centered at a given location as shown on
Figure \ref{fig:blob}. This kind of data are \textit{linearly
  separable}. It means that a straight line (a linear function) is
able to separate (or classify) these kind of data. Note that this property can be generalized for
n-dimensional spaces with hyperplanes. 

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=.8\linewidth]{blob}
    \caption{Example of labeled data generated with the \texttt{make\_blobs(...)} function \label{fig:blob}}
  \end{center}
\end{figure}

\begin{info}
  The current script implements only the feed forward rule. 
\end{info}

\begin{task}
  Complete the script by implementing backward propagation rule. Check if your
  algorithm is able to classify your data.   
\end{task}

\begin{info}
  The correction is given by \dltutodnn{02}{tuto-02\_correction-part1.py}
\end{info}



\subsection*{How it really works ?}
The image below plots the results of the above code. As you can
observe, the Perceptron is able to separate the variable with a
straight line. But.... how it really works?

\begin{center}
  \includegraphics[width=1.\linewidth]{result-perceptron}
\end{center}

Take a look at equation \ref{eq:net-input-perceptron}. We can write
this equation related to the net \textit{input function} with 2 dimensional data$(x_0,  x_1)$ as follow:
\begin{equation}
  a = x_0 w_0 + x_1 w_1 + b\nonumber
\end{equation}
Now, remember that the \textit{activation function} $z(a)$ consists in classifying data
depending on the sign of the input $a$ :
\begin{equation}
  z(a) = \left\{
    \begin{array}{rl}
         1 & \mbox{if } a>0 \\
         0 & \mbox{if }  a<0
    \end{array}
\right.\nonumber
\end{equation}

Here, we can take the frontier limit by posing $a$ nor negative and nor positive, i.e,  $a=0$. It gives:
\begin{equation}
  0 = x_0 w_0 + x_1 w_1 + b\nonumber
\end{equation}
Now, we can express $x_1$ as a function of $x_0$ as:
\begin{equation}
  x_1 = -x_0 \frac{w_0}{w_1} - \frac{b}{w_1} \label{eq:perceptron-line}
\end{equation}
This last equation is linear, we retrieve the well known form $y=mx+y_0$ where:
\begin{equation}
   m= -\frac{w_0}{w_1} \quad \text{and} \quad y_0= -\frac{b}{w_1} \nonumber
\end{equation}
So, if the values of the coefficients $w_0$, $w_1$ and $b$ are known,
we are able to plot the function $x_{1} = f(x_{0})$. The related function is
linear and  the related plot is a straight line. This straight line
separates the space in two half spaces:
\begin{enumerate}
\item the half space below the line corresponds to the case where $a>0$, e.g, the \texttt{'1'} class and
\item the half space above the line corresponds to the case where $a<0$, e.g, the \texttt{'0'} class.
\end{enumerate}

\begin{task}
  To verify the above assumption, you can try to plot the related equation \ref{eq:perceptron-line}.
  You must obtain something like this (dash dot line named \textit{Replot}).
  \begin{center}
  \includegraphics[width=.8\linewidth]{result-perceptron-1}
\end{center}
\end{task}

\begin{info}
  The correction is given by \dltutodnn{02}{tuto-02\_correction-part2.py}
\end{info}




\end{document}
