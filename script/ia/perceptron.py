#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions

# make data
data,label = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# save data if you want to reuse it later 
# data[:,0].tofile('perceptron-x.dat')
# data[:,1].tofile('perceptron-y.dat')
# label.tofile('perceptron-label.dat')

# load the data from files
x  = np.fromfile('perceptron-x.dat')
y  = np.fromfile('perceptron-y.dat')
label = np.fromfile('perceptron-label.dat', dtype=int)
data = np.stack((x, y), axis=-1)


# initialize paramaters
w   = np.zeros(len(data[0])) # it contains (w0,w1)
b   = 0.
eta   = 0.001 # learning rate
epoch = 100
err   = [0] # to monitor the error             


for i in range(epoch):
    # let's compute, we iterate over all the date 
    for x,t in zip(data,label):
        # feed forward 
        a = np.dot(x, w) + b
        z = 1 if a>0. else 0
        y = z

        # backward propagation
        w += eta*(t-y)*x
        b += eta*(t-y)

        # monitor error
        err.append(err[-1] + np.abs(t-y))



    
# the perceptron class, this is mandatory for plotting with mlxtend
class Perceptron:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        a = np.dot(X, w) + b
        return np.where(a >= 0., 1, 0)

# plotting
plt.figure()
plot_decision_regions(data, label, clf=Perceptron)

# check line equation with (see polycop)
# eq_x = np.arange(np.min(data[:,0]), np.max(data[:,0]), 0.01)
# eq_y = -(eq_x*w[0] + b)/w[1]
# plt.plot(eq_x, eq_y, '--', lw=4)

# least square regression (DA: just to see what it gives)
# from scipy import stats
# slope, intercept, r_value, p_value, std_err = stats.linregress(data[:,0],data[:,1])
# plt.plot(eq_x, intercept + slope*eq_x, '-', lw=4)


plt.figure()
plt.plot(err)

plt.show()
