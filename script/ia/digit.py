#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions
import code # use 'code.interact(local=locals())' to get a prompt


def relu(x):
    z = np.copy(x)
    z[z < 0] = 0
    return z

def relu_prime(x):
    z = np.copy(x)
    z[z < 0] = 0
    z[z > 0] = 1
    return z

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)


# make data
digits = datasets.load_digits()
data   = digits.data
target = digits.target

m = len(target[:1000])
t = np.eye(10)[target[:1000]]
x = data[:1000]/16.

m_valid = len(target[1000:])
t_valid = np.eye(10)[target[1000:]]
x_valid = data[1000:]/16.



# number of outputs
N1 = 64 # input
N2 = 20# hidden layer
N3 = 10 # output

# initialize weight
W1 = np.random.random((N1, N2)) / np.sqrt(N1)
W2 = np.random.random((N2, N3)) / np.sqrt(N2)

#initialize bias 
B1 = np.zeros((1, N2))
B2 = np.zeros((1, N3))


# the perceptron class, this is mandatory for plotting with mlxtend
class Perceptron:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        A1 = np.dot(X, W1) + B1
        Z1 = relu(A1)

        A2 = np.dot(Z1, W2) + B2
        Z2 = sigmoid(A2)
        #y = np.argmax(Z2, axis=1)
        return Z2
    @staticmethod
    def predict_digit(X):
        Z2 = Perceptron.predict(X)
        y = np.argmax(Z2, axis=1)
        return y
    
    

eta   = .01 
epoch = 200
mb    = 50 # number of mini-batch

# some useful variable
loss_train_data = np.zeros(epoch)
loss_check_data = np.zeros(epoch)

for i in range(epoch):
    s = np.arange(x.shape[0])
    np.random.shuffle(s)
    x_s = np.split(x[s], mb)
    t_s = np.split(t[s], mb)
    l = 0.
    for X,T in zip(x_s, t_s):
        # feed forward 
        A1 = np.dot(X, W1) + B1
        Z1 = relu(A1)

        A2 = np.dot(Z1, W2) + B2
        Z2 = sigmoid(A2)
        
        y = Z2
        l += (1./m) * np.sum( (y-T)**2 ) # record the current value of loss
        
        ## backward propagation of output layer
        L2  = Z2-T
        M2  = sigmoid_prime(A2)
        ## Compute gradients 
        DW2 = np.dot(Z1.T, L2*M2)
        DB2 = np.sum(L2*M2, axis=0)
        ## Change weights and bias
        W2 -= eta * DW2
        B2 -= eta * DB2
        
        ## backward propagation of hidden layer
        L1 = np.dot(L2*M2, W2.T)
        M_VALID = relu_prime(A1)
        ## Compute gradients 
        DW1 = np.dot(X.T, L1*M_VALID)
        DB1 = np.sum(L1*M_VALID, axis=0)
        ## Change weights and bias
        W1 -= eta * DW1
        B1 -= eta * DB1
        
        #code.interact(local=locals())

    loss_train_data[i] = l
    loss_check_data[i] = (1./m_valid) * np.sum( (Perceptron.predict(x_valid)-t_valid)**2 ) # record the current value of loss
        


n_good_pred = (target[1000:] == Perceptron.predict_digit(data[1000:])).sum()    
print ("good prediction is {0:.2f} %".format(100.* n_good_pred/len(target[1000:])))

    
# plotting
plt.figure()
plt.plot(loss_train_data, '-', label="train")
plt.plot(loss_check_data, '-', label="test")
plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()

