#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions

# if you want to get previous data 
x  = np.fromfile('perceptron-x.dat')
y  = np.fromfile('perceptron-y.dat')
t = np.fromfile('perceptron-label.dat', dtype=int)
x = np.stack((x, y), axis=-1)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)

# make data
x,t = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# standardize the data (a method to get more robust data)
x = (x - x.mean()) / x.std() 

# initialize paramaters
w     = np.random.rand(len(x[0])) # it is better to get random values here
b     = 0.
eta   = .1 
epoch = 1000
mb    = 10 # number of mini-batch

# some useful variable
loss = np.zeros(epoch)
m    = len(t)

for i in range(epoch):
    s = np.arange(x.shape[0])
    np.random.shuffle(s)
    x_s = np.split(x[s], mb)
    t_s = np.split(t[s], mb)
    l = 0.
    for xb,tb in zip(x_s, t_s):
        m = len(t_s)
        
        # feed forward 
        a = np.dot(xb, w) + b
        z = sigmoid(a)
        y = z

        l += (1./m) * np.sum( (y-tb)**2 ) # record the current value of loss

        # backward propagation
        w -= (2./m)*eta * np.dot(((y-tb)*sigmoid_prime(a)).T, xb)
        b -= (2./m)*eta * np.sum( (y-tb)*sigmoid_prime(a))
    loss[i] = l
        
# the perceptron class, this is mandatory for plotting with mlxtend
class Perceptron:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        a = np.dot(X, w) + b
        z = sigmoid(a)
        z = np.where(z >= 0.8, 1, (np.where(z<=0.2, 0, -1)))
        return z

# plotting
plt.figure()
plot_decision_regions(x, t, clf=Perceptron)

plt.figure()
plt.plot(loss, '-')
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()

