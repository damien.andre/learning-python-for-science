import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions

x = np.fromfile('perceptron-x.dat')
y = np.fromfile('perceptron-y.dat')
label = np.fromfile('perceptron-label.dat', dtype=int)
data = np.stack((x, y), axis=-1)

# initialize paramaters
w = np.zeros(2) # it contains (w0,w1)
b = 0.
eta = 0.001 # learning rate

# ... same as before
epoch = 100 # epoch number
err   = [0] # to monitor the error

# let's compute, we iterate over all the date
for i in range(epoch): # ADD A LOOP HERE FOR EPOCH!
    for x,t in zip(data,label):
        # feed forward
        a = np.dot(x, w) + b
        z = 1 if a>0. else 0
        y = z
        #backward propagation
        w += eta*(t-y)*x
        b += eta*(t-y)
        # monitor error
        err.append(err[-1] + np.abs(t-y))

# the perceptron class, this is mandatory for plotting with mlxtend
class Perceptron:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        a = np.dot(X, w) + b
        return np.where(a >= 0., 1, 0)

# plotting
plt.figure()
plot_decision_regions(data, label, clf=Perceptron)

plt.figure()
plt.plot(err)
plt.xlabel("learning step")
plt.ylabel("error")
plt.show()
