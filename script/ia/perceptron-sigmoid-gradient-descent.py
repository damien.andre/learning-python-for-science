#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions

x = np.fromfile('perceptron-x.dat')
y = np.fromfile('perceptron-y.dat')
t = np.fromfile('perceptron-label.dat', dtype=int)
x = np.stack((x, y), axis=-1)

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)

# make data
x,t = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# initialize paramaters
w   = np.zeros(len(x[0])) # it contains (w0,w1)
b   = 0.
eta   = .1 
epoch = 1000

# some useful variable
m    = len(t)
loss = np.zeros(epoch)

for i in range(epoch):
    # feed forward 
    a = np.dot(x, w) + b
    z = sigmoid(a)
    y = z
    
    loss[i] = (1./m) * np.sum( (y-t)**2 ) # record the current value of loss
    
    # backward propagation
    w += -(2./m)*eta * np.dot(((y-t)*sigmoid_prime(a)).T, x)
    b += -(2./m)*eta * np.sum( (y-t)*sigmoid_prime(a))
    
# the perceptron class, this is mandatory for plotting with mlxtend
class Perceptron:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        a = np.dot(X, w) + b
        z = sigmoid(a)
        z = np.where(z >= 0.8, 1, (np.where(z<=0.2, 0, -1)))
        return z

# plotting
plt.figure()
plot_decision_regions(x, t, clf=Perceptron)

plt.figure()
plt.plot(loss, '-')
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()

