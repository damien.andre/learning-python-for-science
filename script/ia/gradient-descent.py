# From calculation, it is expected that the local minimum occurs at x=9/4

cur_x     = 0.2     # where the algorithm starts
eta       = 0.01    # step size multiplier
precision = 0.00001 # the precision to achieve
previous_step_size = 1 

xx   = [] # to plot results

f  = lambda x: x**4 - 3*x**3 + 2
df = lambda x: 4 * x**3 - 9 * x**2

while previous_step_size > precision:
    prev_x = cur_x
    cur_x -= eta * df(prev_x)
    previous_step_size = abs(cur_x - prev_x)
    xx.append(cur_x)

print("The local minimum occurs at", cur_x)



import matplotlib.pyplot as plt
import numpy as np
x = np.arange(-.5, 3, 0.01)
plt.plot(x, f(x))
plt.plot(xx, f(np.array(xx)), 'o')
plt.show()




