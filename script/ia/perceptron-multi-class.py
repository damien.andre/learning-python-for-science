#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
from mlxtend.plotting import plot_decision_regions
import code # for debugging : use 'code.interact(local=locals())' somewhere to get a prompt

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)

class Perceptron:
    def __init__(self, n):
        self.w = np.random.rand(n) 
        self.b   = 0.

    def train(self, x, t, eta, epoch, mb):
        # some useful variable
        self.loss = np.zeros(epoch)

        for i in range(epoch):
            s = np.arange(x.shape[0])
            np.random.shuffle(s)
            x_s = np.split(x[s], mb)
            t_s = np.split(t[s], mb)
            l = 0.
            for xb,tb in zip(x_s, t_s):
                m = len(t_s)

                # feed forward 
                a = np.dot(xb, self.w) + self.b
                z = sigmoid(a)
                y = z

                l += (1./m) * np.sum( (y-tb)**2 ) # record the current value of loss

                # backward propagation
                self.w -= (2./m)*eta * np.dot(((y-tb)*sigmoid_prime(a)).T, xb)
                self.b -= (2./m)*eta * np.sum( (y-tb)*sigmoid_prime(a))
            self.loss[i] = l

    def predict(self, x):
        a = np.dot(x, self.w) + self.b
        z = sigmoid(a)
        return z


def change_target(t, val):
    return np.where(t == val, 1, 0)

x,t = datasets.make_blobs(n_samples=1000, centers=3, n_features=2)
x = (x - x.mean()) / x.std() 

p0 = Perceptron(len(x[0]))
p0.train(x, change_target(t,0) , 0.001, 1000, 10)

p1 = Perceptron(len(x[0]))
p1.train(x, change_target(t,1) , 0.001, 1000, 10)

p2 = Perceptron(len(x[0]))
p2.train(x, change_target(t,2) , 0.001, 1000, 10)


class PLOT:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        r0 = p0.predict(X)
        r1 = p1.predict(X)
        r2 = p2.predict(X)
        r = np.column_stack((r0,r1,r2))
        return np.argmax(r, axis=1)
        
# plotting
plt.figure()
plot_decision_regions(x, t, clf=PLOT)

plt.figure()
plt.plot(p0.loss, label="perceptron p0 = class '0'")
plt.plot(p1.loss, label="perceptron p1 = class '1'")
plt.plot(p2.loss, label="perceptron p2 = class '2'")
plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()
