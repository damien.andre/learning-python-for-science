from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np


f     = lambda x,y: np.sin(0.1*x**2 + 0.05*y**2 + y)
df_dx = lambda x,y: np.cos(0.1*x**2 + 0.05*y**2 + y)*(0.2*x)
df_dy = lambda x,y: np.cos(0.1*x**2 + 0.05*y**2 + y)*(0.1*y+1)

cur_x = -4. # The algorithm starts
cur_y = -.5 # The algorithm starts
gamma = 0.1 # rate
precision = 0.000001
previous_step_size = 1 

# for plotting
xx   = []
yy   = []
while np.all(previous_step_size) > precision:
    prev_x = cur_x
    prev_y = cur_y
    cur_x -= gamma * df_dx(prev_x, prev_y)
    cur_y -= gamma * df_dy(prev_x, prev_y)
    previous_step_size = np.array([abs(cur_x - prev_x), abs(cur_y - prev_y)])

    xx.append(cur_x)
    yy.append(cur_y)

# Make data.
X = np.arange(-5, 5, 0.02)
Y = np.arange(-5, 0, 0.02)
X, Y = np.meshgrid(X, Y)

# plot data    
fig = plt.figure()
ax = fig.gca(projection='3d')
plt.axis('equal')
ax.plot_wireframe(X, Y, f(X,Y), rstride=10, cstride=10,alpha=0.5)
ax.plot_surface(X, Y, f(X,Y), cmap=cm.coolwarm, linewidth=0, antialiased=False, alpha=0.2)
ax.scatter(xx, yy, f(np.array(xx),np.array(yy)), color='red')
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')
plt.show()
