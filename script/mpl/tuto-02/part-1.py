#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import numpy as np
import matplotlib.pyplot as plt


# read data file 
t, p_x, p_y = np.loadtxt("./bouncing-ball.txt", delimiter='\t', usecols=(0, 1, 2), unpack=True)


# plot the ball's trajectory
plt.figure()
plt.plot(p_x, p_y, "o")
plt.xlim(0,500)
plt.ylim(0,100)
plt.gca().set_aspect('equal', adjustable='box')
plt.xlabel("x position (m)")
plt.ylabel("y position (m)")
plt.title("bouncing ball's trajectory")
plt.show()
