#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import numpy as np
import matplotlib.pyplot as plt

# read data file 
t, p_x, p_y = np.loadtxt("./bouncing-ball.txt", delimiter='\t', usecols=(0, 1, 2), unpack=True)

# simulation parameters 
g    = -9.81
p_x0 = 10.
p_y0 = 80.
v_x0 = 10.
v_y0 = 0.

# equation of motion
p_xth = v_x0 * t + p_x0
p_yth = 0.5*g*t*t + p_y0

# plot the ball's trajectory and compare
# with analytical formula 
plt.figure()
plt.plot(p_x, p_y  , "o" , label="simulation data")
plt.plot(p_xth, p_yth, "--", label="analytics" )
plt.xlim(0,50)
plt.ylim(0,100)
plt.xlabel("x position (m)")
plt.ylabel("y position (m)")
plt.title("bouncing ball's trajectory")
plt.legend()


# show all the matplotlib figures
plt.show()
