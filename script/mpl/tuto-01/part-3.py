import numpy as np

x = np.linspace(0, 9.9, 100)
print(x)

y = np.cos(x)


import matplotlib.pyplot as plt

plt.figure()
plt.plot(x,y)
plt.title("mon 1er graphe")
plt.xlabel("abscisse (x)")
plt.ylabel("ordonnée (y)")
plt.show()
