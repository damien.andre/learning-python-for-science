#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import random

word_list=["hello", "world", "linux", "python"]

word = word_list[random.randint(0,len(word_list)-1)]
curr = ['_' for c in word]

while True:
    print ("current word is '{}'".format(curr))
    entry = str(input("give a character: "))[0]
    if entry in word :
        i = 0
        for c in word:
            if c is entry:
                curr[i] = c
            i += 1

        if not '_' in curr:
            print ("YOU WIN, the mystery word was '{}'".format(word))
            break
