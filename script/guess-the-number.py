#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import random

num = random.randint(0,100)

while True:
    entry = int(input("give a number between [0, 100]: "))
    if entry < 0 or entry > 100:
        print ("please enter a number in the range [0, 100]")
    elif entry > num :
        print ("your entry is higher than hidden number")
    elif entry < num :
        print ("your entry is lower than hidden number")
    elif entry == num :
        print ("You guess the number. The mystery number was '{}'".format(num))
        break
