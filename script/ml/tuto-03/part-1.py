import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2

(im_train, lab_train), (im_test , lab_test) = keras.datasets.mnist.load_data()

## normalizing images
im_train = im_train / 255.
im_test  = im_test  / 255.

## size of the vectorized image 
print("the size of vectorized image should be:", 28*28)

## vectorized images
x_train = im_train.reshape((60000, 784))
x_test  = im_test.reshape((10000 , 784))
