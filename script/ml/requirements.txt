# requirements for ml and opencv
tensorflow==2.8.0
numpy==1.22.3
matplotlib==3.5.1
opencv-python==4.5.1.48
opencv-contrib-python==4.5.1.48
scikit-learn==1.0.2
scikit-image==0.19.2
protobuf==3.20.*
mlxtend==0.21.0
thonny==4.0.1 

