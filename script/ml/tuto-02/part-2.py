import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2

(im_train, lab_train), (im_test , lab_test) = keras.datasets.mnist.load_data()

## display the 50th image of the im_train array
plt.figure()
plt.imshow(im_train[50], cmap="gray")
plt.show()

## display the 50th value of the lab_train array
print(lab_train[50])

## display the 50th image of the im_test array
plt.figure()
plt.imshow(im_test[50], cmap="gray")
plt.show()

## display the 50th value of the lab_test array
print(lab_test[50])

