import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2

(im_train, lab_train), (im_test , lab_test) = keras.datasets.mnist.load_data()

## the plot_image function 
def plot_images(images, r,L,C):
    plt.figure(figsize=(C,L))
    for i in range(L*C):
        plt.subplot(L, C, i+1)
        plt.imshow(images[r+i], cmap='gray')
        plt.xticks([]); plt.yticks([])

## plot the train images in a 4x6 grid
plot_images(im_train, 600, 6, 4)
plt.show()

## check the corresponding labels 
print(lab_train[600:624])

## plot the test images in a 4x6 grid
plot_images(im_test, 600, 6, 4)
plt.show()

## check the corresponding labels 
print(lab_test[600:624])


