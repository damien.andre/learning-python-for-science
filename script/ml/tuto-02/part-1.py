import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2

(im_train, lab_train), (im_test , lab_test) = keras.datasets.mnist.load_data()

## check the type of the variable 
print(type(im_train))
print(type(lab_train))
print(type(im_test))
print(type(lab_test))

## check the shape 
print("im_train.shape :", im_train.shape )
print("lab_train.shape:", lab_train.shape)
print("im_test.shape  :", im_test.shape  )
print("lab_test.shape :", lab_test.shape )
