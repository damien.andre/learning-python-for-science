# version 2.1 du 21 mai 2022

import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from mlxtend.plotting import plot_decision_regions
import tensorflow as tf
from tensorflow import keras

def get_already_treated_mnist_data():
    (im_train, lab_train), (im_test , lab_test) = keras.datasets.mnist.load_data()
    # normalizing images
    im_train = im_train / 255.
    im_test  = im_test  / 255.
    
    ## vectorized images
    x_train = im_train.reshape((60000, 784))
    x_test  = im_test.reshape((10000 , 784))
    
    ## conversion in one-hot vector of lab_train
    y_train = tf.keras.utils.to_categorical(lab_train, num_classes=10)
    y_train = tf.constant(y_train, shape=[60000, 10])
    
    ## conversion in one-hot vector of lab_test
    y_test = tf.keras.utils.to_categorical(lab_test, num_classes=10)
    y_test = tf.constant(y_test, shape=[10000, 10])
    return (x_train, y_train), (x_test, y_test)

class FAKE:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        return np.zeros(len(X))

def plot_region(x,y,clf=None):
    if clf is None:
        plot_decision_regions(x,y,clf=FAKE)
    else:
        plot_decision_regions(x,y,clf=clf)
    


def plot_loss_accuracy(history):
    '''Plot training & validation loss & accuracy values, giving an argument
       'history' of type 'tensorflow.python.keras.callbacks.History'. '''
    
    plt.figure(figsize=(15,5))
    ax1 = plt.subplot(1,2,1)
    if history.history.get('accuracy'):
        ax1.plot(np.array(history.epoch)+1, history.history['accuracy'], 'o-',label='Train')
    if history.history.get('val_accuracy'):
        ax1.plot(np.array(history.epoch)+1, history.history['val_accuracy'], 'o-', label='Test')
    ax1.set_title('Model accuracy')
    ax1.set_ylabel('Accuracy')
    ax1.set_xlabel('Epoch') 
    ax1.grid()
    ax1.legend(loc='best')
    
    # Plot training & validation loss values
    ax2 = plt.subplot(1,2,2)
    if history.history.get('loss'):
        ax2.plot(np.array(history.epoch)+1, history.history['loss'], 'o-', label='Train')
    if history.history.get('val_loss'):
        ax2.plot(np.array(history.epoch)+1, history.history['val_loss'], 'o-',  label='Test')
    ax2.set_title('Model loss')
    ax2.set_ylabel('Loss')
    ax2.set_xlabel('Epoch')
    ax2.legend(loc='best')
    ax2.grid()
    plt.show()

def plot_images(image_array, r, L, C):
    '''Plot the images of image_array on a grid L x C, starting at
       rank r'''
    plt.figure(figsize=(C,L))
    for i in range(L*C):
        plt.subplot(L, C, i+1)
        plt.imshow(image_array[r+i], cmap='gray')
        plt.xticks([]); plt.yticks([])


def compute_confusion_matrix(true, results, classes):
    ''' This function plot the confusion matrix.
        true    : the actual labels 
        results : the labels computed by the trained network (one-hot format)
        classes : list of possible label values'''
    predicted = np.argmax(results, axis=-1) # tableau d'entiers entre 0 et 9 
    cm = confusion_matrix(true, predicted)
    return cm

