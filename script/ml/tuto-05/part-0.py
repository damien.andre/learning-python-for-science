import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras import layers
import mltool

(im_train, lab_train), (im_test, lab_test) = keras.datasets.mnist.load_data()
( x_train,   y_train), ( x_test,   y_test) = mltool.get_already_treated_mnist_data()

## loading the neural network which is located in the 'DNN' folder
model = keras.models.load_model('DNN')
