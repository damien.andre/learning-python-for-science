import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras import layers
import mltool

(im_train, lab_train), (im_test, lab_test) = keras.datasets.mnist.load_data()
( x_train,   y_train), ( x_test,   y_test) = mltool.get_already_treated_mnist_data()

## loading the neural network which is located in the 'DNN' folder
model = keras.models.load_model('DNN')

## use the neural network prediction
res = model.predict(x_test)

## post-treat the result
res = np.argmax(res, axis=1)

## compare the result to the label
res = res == lab_test

## extract numbers of good and bad predictions 
number_of_good_prediction = np.sum(res)
number_of_bad_prediction  = 10000 - number_of_good_prediction

## display the result
percentage = 100*(number_of_good_prediction / 10000)
print("The neural network has {:.2f}% of good prediction".format(percentage))

