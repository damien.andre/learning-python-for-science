import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras import layers
import mltool


(im_train, lab_train), (im_test, lab_test) = keras.datasets.mnist.load_data()
( x_train,   y_train), ( x_test,   y_test) = mltool.get_already_treated_mnist_data()

## loading the neural network which is located in the 'DNN' folder
model = keras.models.load_model('DNN')

## use the neural network prediction
res = model.predict(x_test)

## print the confusion matrix
print("")
print("absolute confusion matrix")
print("=========================")
cm = mltool.compute_confusion_matrix(lab_test, res, range(10))
print(cm)

## display the number of image per classes
print("")
print("number of image per classes")
print("===========================")
for i in range(10):
    print("there is {:04d} images of '{}'".format(np.sum(lab_test == i), i))

## build a custom version of the confusion matrix
cm = cm.astype(float) # change type 
for i in range(10):
    tot = np.sum(cm[i])
    cm[i,:] *= 100/tot 
float_formatter = "{:.2f}".format
np.set_printoptions(formatter={'float_kind':float_formatter})
## and use it 
print("")
print("relative confusion matrix (%)")
print("==============================")
print(cm)





