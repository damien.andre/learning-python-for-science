import tensorflow as tf
from tensorflow import keras
import numpy as np
import sys
import matplotlib.pyplot as plt
import cv2
from tensorflow.keras import layers
import mltool

(im_train, lab_train), (im_test, lab_test) = keras.datasets.mnist.load_data()
( x_train,   y_train), ( x_test,   y_test) = mltool.get_already_treated_mnist_data()

## loading the neural network which is located in the 'DNN' folder
model = keras.models.load_model('DNN')

## display the 50th image of the test data
plt.imshow(im_test[50], cmap="gray")
plt.show()

## use the neural network prediction
res = model.predict(x_test[50:51])
print(res)
print("The neural network predict that this image is:", res.argmax())
