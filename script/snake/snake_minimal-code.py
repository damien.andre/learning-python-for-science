import curses
from curses import wrapper
import time
# haut   -> 259
# bas    -> 258
# gauche -> 260
# droite -> 261
# q      -> 113




def main(stdscr):
    direction = 261
    stdscr.clear()
    stdscr.refresh()
    stdscr.nodelay(True)
    curses.curs_set(False)
    x, y = 0, 0
    coord = []
    
    while True:
        d = stdscr.getch()
        if d == 261 or d == 260 or d==258 or d==259:
            direction = d

        if direction == 261:
            x = x + 1        
        elif direction == 260:
            x = x - 1
        elif direction == 258:
            y = y + 1
        elif direction == 259:
            y = y - 1
        
        stdscr.addstr(y, x, "o")
        stdscr.refresh()
        time.sleep(0.1)

wrapper(main)
