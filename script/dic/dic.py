#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

import matplotlib
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 30}
matplotlib.rc('font', **font)


# image size
img_size = (2560, 1920)

# zone of interest size 
size = (116, 30)
sx   = size[0]
sy   = size[1]

# init numpy array 
pos_x  = np.zeros(size)
pos_y  = np.zeros(size)
disp_x = np.zeros(size)
disp_y = np.zeros(size)


# read the file and put data in numpy array
with open('./dic.csv', 'r') as f:
    for n, line in enumerate(f):
        line = line.rstrip().split(',')
        if n is not 0:
            i,j = int(line[1]), int(line[2])
            pos_x[i,j]  = float(line[3])
            pos_y[i,j]  = float(line[4])
            disp_x[i,j] = float(line[5])
            disp_y[i,j] = float(line[6])

# apply scale
scale  = 6.1e-06 # meter/pixel
pos_x  *= scale
pos_y  *= scale
disp_x *= scale
disp_y *= scale

            
# plot displacement field
plt.figure()
plt.quiver(pos_x, pos_y, disp_x, disp_y, width=0.001)
plt.axis('equal')
plt.xlim(0, 2560*scale)
plt.ylim(0, 1920*scale)
plt.title('Correlated displacement field')
plt.xlabel('width (m)')
plt.ylabel('height (m)')


# compute strain with np.gradient
dx = pos_x[1,0] - pos_x[0,0]
strain_xx = np.gradient(disp_x, dx, axis=0)

# plot the strain map 
plt.figure()
CS = plt.contourf(pos_x, pos_y, strain_xx, 10, cmap=plt.cm.gist_heat)
cbar = plt.colorbar(CS)
plt.axis('equal')
plt.xlim(0, 2560*scale)
plt.ylim(0, 1920*scale)
plt.title('Strain xx')
plt.xlabel('width (m)')
plt.ylabel('height (m)')







# Now, let's try to compute Young's modulus with two methods
# first, some constants must be computed 
L = (41. - 5.)*1e-3  # distance between the upper supports
l = (19. - 5.)*1e-3  # distance between the lower supports
b = 7.66e-3          # sample width
h = 4.06e-3          # sample height
F = 400.             # the applied force
Mf = (F/4)*(L-l)     # the applied torque
I  = (b*h**3)/12     # inertia momentum



# get the evolution of the y displacement along the horizontal axis in the middle of sample
# the material strength theory tell us that it must be described by a second order function
x = pos_x[:,sy//2]
y = disp_y[:,sy//2]

# use the non linear least square method to fit data with a second order function 
def f2(x, a, b, c):
    return a*x*x + b*x + c
(a,b,c), pcov = curve_fit(f2, x, y)
ls = f2(x, a, b, c)

# now, let's plot data and the fitted function
plt.figure()
plt.plot(x*1e3, y*1e3, 'o', label="raw data")
plt.plot(x*1e3, ls*1e3, '--', lw=2, label="fitted function")
plt.legend()
plt.title('deviation of the neutral fiber')
plt.xlabel('length (mm)')
plt.ylabel('deviation (mm)')
plt.show()

# compute young's modulus thanks to the fitted function
E  = -Mf/(2*a*I)
print("Method 2, Young's modulus is E={:.2f} GPa".format(E*1e-9))
