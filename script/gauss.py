#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle

import numpy as np
import matplotlib.pyplot as plt

import matplotlib
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 30}
matplotlib.rc('font', **font)


def gauss(x, K):
    return np.exp(-(x-0.5)**2/(K**2))

x = np.linspace(0,1,1000)
K = np.linspace(0.1, 0.9, 8)

plt.figure()

for k in K:
    y = gauss(x, k)
    plt.plot(x, y, label="K={0:.2f}".format(k))

plt.legend()
plt.show() 
