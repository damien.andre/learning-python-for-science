import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
import mltool

# make randomized data
data,label = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# plot data
plt.figure()
mltool.plot_region(data, label)
plt.show()
