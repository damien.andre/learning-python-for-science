cur_x = 0.2
# where the algorithm starts
eta = 0.01
# step size multiplier
precision = 0.00001 # the precision to achieve
previous_step_size = 1

def f(x):
    return x**4 - 3*x**3 + 2

def df(x):
    return 4*x**3 - 9*x**2

while previous_step_size > precision:
    prev_x = cur_x
    cur_x -= eta * df(prev_x)
    previous_step_size = abs(cur_x - prev_x)
print("The local minimum occurs at", cur_x)
