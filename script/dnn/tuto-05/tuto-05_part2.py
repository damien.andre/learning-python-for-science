import numpy as np

cur_x = -4.0 # The algorithm starts
cur_y = -0.5 # The algorithm starts
gamma = 0.1 # rate
precision = 0.000001
previous_step_size = np.array([10000, 10000])

def f(x,y):
    #....
    
def df_dx(x,y):
    #....

def df_dy(x,y):
    #....
    

while np.all(previous_step_size) > precision:
    #.... the algorithm loop

print("The local minimum occurs at", cur_x, cur_y)
