import numpy as np

cur_x = -4.0 # The algorithm starts
cur_y = -0.5 # The algorithm starts
gamma = 0.1 # rate
precision = 0.000001
previous_step_size = np.array([10000, 10000])

def f(x,y):
    return np.sin(0.1*x**2 + 0.05*y**2 + y)

def df_dx(x,y):
    return np.cos(0.1*x**2 + 0.05*y**2 + y)*(0.2*x)

def df_dy(x,y):
    return np.cos(0.1*x**2 + 0.05*y**2 + y)*(0.1*y+1)

while np.all(previous_step_size) > precision:
    prev_x = cur_x
    prev_y = cur_y
    cur_x -= gamma * df_dx(prev_x, prev_y)
    cur_y -= gamma * df_dy(prev_x, prev_y)
    previous_step_size = np.array([abs(cur_x - prev_x), abs(cur_y - prev_y)])

print("The local minimum occurs at", cur_x, cur_y)
