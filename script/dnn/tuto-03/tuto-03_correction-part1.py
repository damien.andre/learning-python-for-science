import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
import mltool

# make data
data,label = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)
# initialize paramaters
w = np.zeros(2) # it contains (w0,w1)
b = 0.
eta = 0.001 # learning rate
err = [0.]   # cumulative error

# let's compute, we iterate over all the date
for x,t in zip(data,label):
    # feed forward
    a = np.dot(x, w) + b
    z = 1 if a>0. else 0
    y = z

    # backward propagation
    w += eta*(t-y)*x
    b += eta*(t-y)

    # add to the cumulative error
    err.append(err[-1] + np.abs(t-y))
    
def predict(X): # here, X is a numpy array that contains all the data
    a = np.dot(X, w) + b
    return np.where(a >= 0., 1, 0)

# plotting
plt.figure()
mltool.plot_region(data, label, clf=predict)

plt.figure()
plt.plot(err)
plt.xlabel("step")
plt.ylabel("error")
plt.show()
