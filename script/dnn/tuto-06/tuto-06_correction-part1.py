#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
import mltool


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)

# make data
data,label = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# initialize paramaters
w   = np.zeros(2) # it contains (w0,w1)
b   = 0.
eta   = .1 
epoch = 100

# some useful variable
m  = len(data)  # number of data 
cum_loss = []   # cumulative loss per epoch 

for e in range(epoch):
    print("epoch", e)
    loss = 0.
    for x,t in zip(data,label):

        # feed forward 
        a = np.dot(x, w) + b
        z = sigmoid(a)
        y = z

        # record the lossu
        loss += (y-t)**2 #+ loss[i-1]

        # backward propagation
        w -= eta*2*(y-t)*sigmoid_prime(a)*x
        b -= eta*2*(y-t)*sigmoid_prime(a)
        
    cum_loss.append((loss/m)*100.)
    
def predict(X): # here, X is a numpy array that contains all the data
    a = np.dot(X, w) + b
    z = sigmoid(a)
    z = np.where(z >= 0.5, 1, (np.where(z<0.5, 0, -1)))
    return z

# plotting
plt.figure()
mltool.plot_region(data, label, clf=predict)
plt.figure()
plt.plot(cum_loss, '-')
plt.xlabel('epoch')
plt.ylabel('cumulative loss (%)')
plt.show()

