import numpy as np

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)

class Perceptron:
    def __init__(self, n):
        self.w = np.random.rand(n) 
        self.b = np.random.rand(1) 

    def train(self, x, t, eta, epoch, mb):
        # some useful variable
        self.loss = np.zeros(epoch)

        for i in range(epoch):
            s = np.arange(x.shape[0])
            np.random.shuffle(s)
            x_s = np.split(x[s], mb)
            t_s = np.split(t[s], mb)
            l = 0.
            for xb,tb in zip(x_s, t_s):
                m = len(t_s)

                # feed forward 
                a = np.dot(xb, self.w) + self.b
                z = sigmoid(a)
                y = z

                l += (1./m) * np.sum( (y-tb)**2 ) # record the current value of loss

                # backward propagation
                self.w -= (2./m)*eta * np.dot(((y-tb)*sigmoid_prime(a)).T, xb)
                self.b -= (2./m)*eta * np.sum( (y-tb)*sigmoid_prime(a))
            self.loss[i] = l

    def predict(self, x):
        a = np.dot(x, self.w) + self.b
        z = sigmoid(a)
        return z


