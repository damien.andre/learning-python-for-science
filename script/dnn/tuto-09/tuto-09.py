#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
import mltool


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)

# make data
x,t = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# initialize paramaters
w   = np.zeros(len(x[0])) # it contains (w0,w1)
b   = 0.
eta   = .1 
epoch = 1000

# some useful variable
loss = np.zeros(epoch) # one loss value per epoch
mb   = 10              # number of mini-batch

for i in range(epoch):
    s = np.arange(x.shape[0])
    np.random.shuffle(s)
    x_s = np.split(x[s], mb)
    t_s = np.split(t[s], mb)
    l = 0.
    for xb,tb in zip(x_s, t_s):
        m = len(t_s)
        
        # feed forward 
        a = np.dot(xb, w) + b
        z = sigmoid(a)
        y = z
        
        # record the loss
        l += (1./m) * np.sum( (y-tb)**2 ) # record the current value of loss
        
        # backward propagation
        w += -(2./m)*eta * np.dot(((y-tb)*sigmoid_prime(a)).T, xb)
        b += -(2./m)*eta * np.sum( (y-tb)*sigmoid_prime(a))
    loss[i] = l

def predict(X): # here, X is a numpy array that contains all the data
    a = np.dot(X, w) + b
    z = sigmoid(a)
    z = np.where(z >= 0.8, 1, (np.where(z<=0.2, 0, -1)))
    return z

# plotting
plt.figure()
mltool.plot_region(x, t, clf=predict)
plt.figure()
plt.plot(loss, '-')
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()

