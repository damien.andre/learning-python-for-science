#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
import mltool
import perceptron


def change_target(t, val):
    return np.where(t == val, 1, 0)

x,t = datasets.make_blobs(n_samples=1000, centers=3, n_features=2)
x = (x - x.mean()) / x.std() 

p0 = perceptron.Perceptron(len(x[0]))
p0.train(x, change_target(t,0) , 0.001, 1000, 10)

p1 = perceptron.Perceptron(len(x[0]))
p1.train(x, change_target(t,1) , 0.001, 1000, 10)

p2 = perceptron.Perceptron(len(x[0]))
p2.train(x, change_target(t,2) , 0.001, 1000, 10)


def predict(X): # here, X is a numpy array that contains all the data
    r0 = p0.predict(X)
    r1 = p1.predict(X)
    r2 = p2.predict(X)
    r = np.column_stack((r0,r1,r2))
    return np.argmax(r, axis=1)
        
# plotting
plt.figure()
mltool.plot_region(x, t, clf=predict)

plt.figure()
plt.plot(p0.loss, label="perceptron p0 = class '0'")
plt.plot(p1.loss, label="perceptron p1 = class '1'")
plt.plot(p2.loss, label="perceptron p2 = class '2'")
plt.legend()
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()


