#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
import mltool

def sigmoid(x):
    # ... to complete ...

def sigmoid_prime(x):
    # ... to complete ...

# make data
x,t = datasets.make_blobs(n_samples=1000, centers=2, n_features=2)

# initialize paramaters
w   = np.zeros(len(x[0])) # it contains (w0,w1)
b   = 0.
eta   = .1 
epoch = 1000

# some useful variable
m    = len(t)
loss = np.zeros(epoch) # one loss value per epoch

for i in range(epoch):
    # feed forward
    # ... to complete ...

    # record the loss
    loss[i] = # ... to complete ..
    
    # backward propagation
    # ... to complete ...
    

def predict(X): 
    a = np.dot(X, w) + b
    z = sigmoid(a)
    z = np.where(z >= 0.8, 1, (np.where(z<=0.2, 0, -1)))
    return z

# plotting
plt.figure()
mltool.plot_region(x, t, clf=predict)
plt.figure()
plt.plot(loss, '-')
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()

