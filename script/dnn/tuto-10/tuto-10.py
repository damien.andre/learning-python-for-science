#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
from sklearn import datasets
from matplotlib import pyplot as plt
import mltool


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_prime(x):
    z = sigmoid(x)
    return z * (1 - z)

# make data
x,label = datasets.make_moons(n_samples=1000, noise=0.1)
x = (x - x.mean()) / x.std() 
t = np.array([label])
t = t.astype(float)
t = t.T

# initialize weight
W1 = np.random.random((len(x[0]), 3))
W2 = np.random.random((3, 1))

#initialize bias 
B1 = np.zeros((1, 3))
B2 = np.zeros((1, 1))



# some useful variable
mb    = 10             # number of mini-batch
eta   = .1             # learning rate
epoch = 1000           # number of epoch 
loss = np.zeros(epoch) # one loss value per epoch



for i in range(epoch):
    s = np.arange(x.shape[0])
    np.random.shuffle(s)
    x_s = np.split(x[s], mb)
    t_s = np.split(t[s], mb)
    l = 0.
    for X,T in zip(x_s, t_s):
        m = len(t_s)
        
        # feed forward
        # ... continue here ...

    loss[i] = l

def predict(X): # here, X is a numpy array that contains all the data
    A1 = np.dot(X, W1) + B1
    Z1 = sigmoid(A1)
    A2 = np.dot(Z1, W2) + B2
    Z2 = sigmoid(A2)
    y = Z2
    return y

# plotting
plt.figure()
mltool.plot_region(x, label, clf=predict)
plt.figure()
plt.plot(loss, '-')
plt.xlabel('epoch')
plt.ylabel('loss')
plt.show()

