import numpy as np
import matplotlib.pyplot as plt
from mlxtend.plotting import plot_decision_regions


class FAKE:
    @staticmethod
    def predict(X): # here, X is a numpy array that contains all the data
        return np.zeros(len(X))

class FAKE1:
    pass
    
def plot_region(x,y,clf=None):
    if clf is None:
        plot_decision_regions(x,y,clf=FAKE)
    else:
        setattr(FAKE1, "predict", clf)
        plot_decision_regions(x,y,clf=FAKE1)
    


