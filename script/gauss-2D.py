#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle


import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import code # for debugging : use 'code.interact(local=locals())' somewhere to get a prompt


def gauss(x, mu, sigma):
    return np.exp(-0.5*((x-mu)/sigma)**2)/(np.sqrt(2*np.pi)*sigma)

def gauss_2D(x, y, mux, muy, sigx, sigy):
    return np.dot(gauss(y,muy,sigy), gauss(x,mux,sigx))

x = np.arange(0,15,0.1)
y = np.arange(0,15,0.1)


# add dimension 
x = x[np.newaxis,:] 
y = y[:,np.newaxis]


g = gauss_2D(x,y,5,7.5,1,3)

# plot it 
plt.imshow(g)
plt.show()
