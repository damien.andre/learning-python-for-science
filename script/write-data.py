#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre


import math as m

with open('data-output.txt', 'w') as f:
    f.write("x \t cos(x) \t sin(x)\n")
    for i in range(10):
        x = i/10.
        l = "{} \t {} \t {}\n".format(x, m.cos(x), m.sin(x))
        f.write(l)
