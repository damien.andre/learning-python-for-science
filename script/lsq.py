#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle


import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import code # for debugging : use 'code.interact(local=locals())' somewhere to get a prompt
from scipy import optimize

x,y = np.loadtxt("lsq.txt", unpack = True)


# The linear_func function, the optimizer will find the best value of a and b
def linear_func(x, a, b):
    return a*x + b

# use least square regression with linear_func_function
pfit, pvariance = optimize.curve_fit(linear_func, x, y, [0, 1]) # [0,1] sont les estimations
print("measured    slope = {:.2f} ; measured    intercept = {:.2f}".format(pfit[0],pfit[1]))
print("theoritical slope = {:.2f} ; theoritical intercept = {:.2f}".format(2,1))
err = np.sqrt((np.diag(pvariance)))


fit = linear_func(x, *pfit)
lim_h = linear_func(x, *(pfit+err))
lim_b = linear_func(x, *(pfit-err))
plt.plot(x,y, 'ok', x, fit)
plt.fill_between(x, lim_b, lim_h, color = 'black', alpha = 0.15)
plt.xlabel('x')
plt.xlabel('y')
plt.show()
