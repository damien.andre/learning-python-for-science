#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle


import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

## read image 
img = mpimg.imread('./cow.png')
plt.figure()
plt.imshow(img)

## get image shape 
print("image shape :", img.shape)

## function for color to gray conversion
def rgb2gray(rgb_im):
    r, g, b = rgb_im[:,:,0], rgb_im[:,:,1], rgb_im[:,:,2]
    gray_im = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray_im


## show image in gray scale
bw = rgb2gray(img)
plt.figure()
plt.imshow(bw, cmap='gray', vmin=0., vmax=1.) 


## zoom to a given part
zoom = bw[200:500, 200:500]
plt.figure()
plt.imshow(zoom, cmap='gray', vmin=0., vmax=1.) 


## tresholding
zoom[zoom < 0.5] = 0
zoom[zoom > 0.5] = 1.
plt.figure()
plt.imshow(zoom, cmap='gray', vmin=0., vmax=1.) 


## crop the figure with a black canva 
crop = np.zeros(bw.shape)
crop[200:500, 200:500] = zoom
plt.figure()
plt.imshow(crop, cmap='gray', vmin=0., vmax=1.) 


## hide a part of the image
bw[200:500, 200:500].fill(0)
plt.figure()
plt.imshow(bw, cmap='gray', vmin=0., vmax=1.) 


plt.show()
