#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre


import matplotlib.pyplot as plt
import numpy as np

####################
# Vertical shading #
####################


y = np.linspace(0., 1., 100) # create 1D array [0.00, 0.01, 0.02, ..., 1.00]
x = np.ones(100)             # create 1D array [1.00, 1.00, 1.00, ..., 1.00]

x = x[np.newaxis,:] # build a line matrix
y = y[:,np.newaxis] # build a column matrix
m = np.dot(y, x)    #  build a matrix thanks to the dot product

plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()



#####################
# Spherical shading #
#####################

x = np.linspace(0., np.pi, 100)
y = np.linspace(0., np.pi, 100)

x = x[np.newaxis,:] 
y = y[:,np.newaxis]
m = np.dot(np.sin(y), np.sin(x))

plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()


#######################################
# Spherical shading 10 times repeated #
#######################################

x = np.tile(np.linspace(0., np.pi, 100), 10)
y = np.tile(np.linspace(0., np.pi, 100), 10)

x = x[np.newaxis,:] 
y = y[:,np.newaxis]
m = np.dot(np.sin(y), np.sin(x))

plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()
