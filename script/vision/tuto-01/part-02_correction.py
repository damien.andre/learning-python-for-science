#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import matplotlib.pyplot as plt

# method 1: build a black image using two imbricated loops
m = []
for _ in range(600):
    l = []
    m.append(l)
    for _ in range(800):
        l.append(0.)

plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()

# method 2: build a black image using a more pythonic (the '*' operator)
m = [[0.]*800]*600
plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()

# method 3 : build a black image using numpy arrays
import numpy as np
m = np.zeros((600, 800))
plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()

# method 4 : build a random 800x600 image using numpy
import numpy as np
m = np.random.rand(600, 800)
plt.imshow(m, cmap='gray', vmin=0., vmax=1.)
plt.show()
