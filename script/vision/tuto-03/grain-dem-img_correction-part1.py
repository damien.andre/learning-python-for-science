#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from scipy import ndimage

##########
# PART 1 #
##########

## function for color to gray conversion
def rgb2gray(rgb_im):
    r, g, b = rgb_im[:,:,0], rgb_im[:,:,1], rgb_im[:,:,2]
    gray_im = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray_im

## read image and convert it
ini_img = mpimg.imread('grain-dem-img.png')
ini_img = rgb2gray(ini_img)

## display original image in grayscale
plt.figure()
plt.imshow(ini_img, cmap='gray', vmin=0., vmax=1.)
plt.show()

## copy initial image
img = ini_img.copy()

## we apply tresholding
img[img<0.5] = 0
img[img>0.5] = 1
plt.imshow(img, cmap='gray', vmin=0., vmax=1.)
plt.show()

## crop image 
grain = img[1020:1180, 1556:1728]
plt.imshow(grain, cmap='gray', vmin=0., vmax=1.)
plt.show()

