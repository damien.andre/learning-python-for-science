import numpy as np
import cv2

cap = cv2.VideoCapture(0)


# circle parameters
radius = 20
x , y = (radius, radius)
color = (0, 255, 0)
thickness = 5

# start animation with the keyboard 
start_move = False

while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # get image width and height
    h,w = np.shape(frame)[0:2]

    # binarize image
    treshold = 80
    gray[gray < treshold] = 0
    gray[gray >=treshold] = 255

    # get the position of the black spot
    max_gray_pos  = np.argwhere(gray==0)
    mean_gray_pos = np.mean(max_gray_pos, axis=0)

    # overwrite the x,y position of the circle
    y , x = int(mean_gray_pos[0]), int(mean_gray_pos[1])

    # draw circle
    frame = cv2.circle(frame, (x,y), radius, color, thickness)
        
    # display image
    cv2.imshow('frame', frame)

    # get keyboard input
    key = cv2.waitKey(1)
    if key == ord('q'):   # press 'q' for quit 
        break

cap.release()
cv2.destroyAllWindows()
