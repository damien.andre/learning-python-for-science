import cv2
from cv2 import aruco
import numpy as np


aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_50)
parameter  = aruco.DetectorParameters()

cap = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_SIMPLEX

while(True):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    corner, num, rejected_point = aruco.detectMarkers(gray, aruco_dict, parameters=parameter)
    frame = aruco.drawDetectedMarkers(frame, corner, num)

    # draw marker number 
    text = "marker : "
    if num is not None:
        for n in num:
            text += str(n[0]) + " " 
    frame = cv2.putText(frame, text, (20,50), font, 1, (0,100,0), 2, cv2.LINE_AA)

    # draw corners
    for ccc in corner:
        for cc in ccc:
            for c in cc:
                frame = cv2.circle(frame, (int(c[0]), int(c[1])), 10, (0,100,255), 5)
    
    cv2.imshow('frame', frame)

    if cv2.waitKey(1) == ord('q'):   
        break

cap.release()
cv2.destroyAllWindows()
