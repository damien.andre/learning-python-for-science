#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from scipy import ndimage


def rgb2gray(rgb_im):
    r, g, b = rgb_im[:,:,0], rgb_im[:,:,1], rgb_im[:,:,2]
    gray_im = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray_im

## read image in grayscale 
img = mpimg.imread('grain.png')
img = rgb2gray(img)

# display image in grayscale for measuring scale
plt.figure()
plt.imshow(img, cmap='gray', vmin=0., vmax=1.)
plt.show()

# pixel to meter conversion
pix2meter = (968-684)/200.

# crop image by removing the info bar at the bottom
img = img[0:680, :]

# display image in grayscale
plt.figure()
plt.imshow(img, cmap='gray', vmin=0., vmax=1.)
plt.show()

# now, we will work on a copy of image
img1 = img.copy()

# apply trehsolding and force the type to be int
treshold = 95/255
img1[img1 < treshold] = 0
img1[img1 >=treshold] = 1
img1 = img1.astype(int)

## display the tresholding image
plt.figure()
plt.imshow(img1, cmap='gray', vmin=0., vmax=1.)
plt.show()

## close the hole
img1 = ndimage.binary_fill_holes(img1)
plt.imshow(img1, cmap='gray', vmin=0., vmax=1.)
plt.show()

## apply binary_erosion 
img1 = ndimage.binary_erosion(img1, iterations=5)
plt.figure()
plt.imshow(img1, cmap='gray', vmin=0., vmax=1.) 
plt.show()

## now, get the labeled objects 
img_lb, N = ndimage.label(img1)

radius    = []
for i in range(1, N+1):
    if i % 100 == 0:
        print("computing grain {}/{}".format(i, N+1))
    im = img_lb.copy()
    # apply a filter to display only one grain 
    im[im != i] = 0
    im[im == i] = 1
    # compute bounding box 
    x_array = np.nonzero(np.argmax(im, axis=1))[0]
    y_array = np.nonzero(np.argmax(im, axis=0))[0]
    # in this cas the length of the returned array can be zero
    if  len(x_array) > 0 and len(y_array) > 0:
        xmax = np.max(x_array)
        xmin = np.min(x_array)
        ymax = np.max(y_array)
        ymin = np.min(y_array)
        # compute length along x and y
        lx = xmax-xmin
        ly = ymax-ymin
        # get max length
        # get diameter by averaging lx and ly
        diam = (lx + ly) /2.
        # compute grain center and display it
        x,y = np.int_(ndimage.center_of_mass(im))
        img[x-1:x+2,y] = 1
        img[x,y-1:y+2] = 1
        # draw a bounding box around the grain
        img[xmin:xmax+1, ymin] = 1
        img[xmin:xmax+1, ymax] = 1
        img[xmin, ymin:ymax+1] = 1
        img[xmax, ymin:ymax+1] = 1
        # record the radius 
        radius.append(diam/2)

         
# display the image
plt.figure()
plt.imshow(img, cmap='gray', vmin=0., vmax=1)
plt.show()

## print statistical info on grain radii
radius = np.array(radius)*pix2meter # convert the list in numpy array 
print("detecting {} grains".format(len(radius)))
print("mean grain radius is {:.2f} µm".format(radius.mean()))
print("std  grain radius is {:.2f} µm".format(radius.std()))

