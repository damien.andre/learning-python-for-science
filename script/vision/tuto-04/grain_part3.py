#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from scipy import ndimage


def rgb2gray(rgb_im):
    r, g, b = rgb_im[:,:,0], rgb_im[:,:,1], rgb_im[:,:,2]
    gray_im = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray_im

## read image in grayscale 
img = mpimg.imread('grain.png')
img = rgb2gray(img)
img = img[0:680, :]


## finding the contour thanks to the gradient trick
grad_x, grad_y = np.abs(np.gradient(img))
img_grad = (grad_x + grad_y)/2
plt.imshow(img_grad, cmap='gray', vmin=0., vmax=1.)
plt.show()


# apply a treshold on this image 
treshold = 15/255
img_grad[img_grad < treshold] = 0
img_grad[img_grad >=treshold] = 1
img_grad = img_grad.astype(int)
plt.imshow(img_grad, cmap=plt.get_cmap('gray')) 
plt.show()

# inverting the white and the black
img_grad = 1-img_grad
plt.imshow(img_grad, cmap=plt.get_cmap('gray')) 
plt.show()

# get back the previous image (given at the first part)
img1 = img.copy()
treshold = 95/255
img1[img1 < treshold] = 0
img1[img1 >=treshold] = 1
img1 = ndimage.binary_fill_holes(img1)
plt.imshow(img1, cmap='gray', vmin=0., vmax=1.)
plt.show()


## go back to the prevous image and apply the mask from the gradient image
img1[img_grad==0] = 0
plt.figure()
plt.imshow(img1, cmap=plt.get_cmap('gray')) 
plt.show()

## fill again the hole 
img1 = ndimage.binary_fill_holes(img1)
plt.imshow(img1, cmap=plt.get_cmap('gray')) 
plt.show()


