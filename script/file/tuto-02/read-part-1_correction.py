#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre


filename = 'img/Fissuration-TiAl2O5__036_-_01490.tif'

print("reading", filename, "...")    
f = open(filename, 'r', encoding='ascii',errors='ignore')
t = None
p = None
for line in f:
    if 'Temperature' in line:
        t = float(line.replace('Temperature=', ''))
    if 'Pressure' in line:
        p = float(line.replace('ChPressure=', ''))

print ('temperature = {} °C'.format(t))
print ('pressure    = {} Pa'.format(p))
f.close()
