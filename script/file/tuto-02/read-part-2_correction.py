#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import glob

file_list   = []
temperature = []
pressure    = []

for filename in glob.glob('img/*.tif'):
    print("reading", filename, "...")    
    
    f = open(filename, 'r', encoding='ascii',errors='ignore')
    t = None
    p = None
    for line in f:
        if 'Temperature' in line:
            t = float(line.replace('Temperature=', ''))
        if 'Pressure' in line:
            p = float(line.replace('ChPressure=', ''))

    file_list.append(filename)
    temperature.append(t)
    pressure.append(p)
    print ('temperature = {} °C'.format(t))
    print ('pressure    = {} Pa'.format(p))
    f.close()

data = open("img.txt", 'w')
data.write("Filename \t Temperature \t Pressure \n")
for i in range(len(file_list)):
    line = "{} \t {} \t {} \n".format(file_list[i], temperature[i], pressure[i])
    data.write(line)
data.close()
