#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import matplotlib.pyplot as plt

it     = []
force  = []

f = open('data.txt', 'r')
for line in f:
    if not '#' in line:
        data = line.split()
        it.append(int(data[0]))
        force.append(float(data[1]))
f.close()

plt.figure()
plt.plot(it, force, 'o-')
plt.show()

