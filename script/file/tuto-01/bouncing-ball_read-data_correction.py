#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import matplotlib.pyplot as plt

pos_x  = []
pos_y  = []

f = open('bouncing-ball_data.txt', 'r')
for line in f:
    data = line.split()
    pos_x.append(float(data[0]))
    pos_y.append(float(data[1]))
f.close()

plt.figure()
plt.plot(pos_x, pos_y, 'o-')
plt.show()

