#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2022, Damien Andre

import math as m
f = open('data-output.txt', 'w')
for i in range(10):
    x = i/10.
    l = "{} \t {} \t {} \n".format(x, m.cos(x), m.sin(x))
    f.write(l)
f.close()
