#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre


import minidem as dem
import random

dt = 0.004
t  = 0
grain = None
    
def apply_gravity():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., -9.81*gr.mass)
        
def manage_contact():
    l = dem.lcm.compute_colliding_pair()
    for (gr1,gr2) in l:
        dem.contact(gr1,gr2)

def velocity_verlet():
    global t, dt
    t = t + dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

def apply_boundaries():
    for gr in dem.simu.grain_list:

        if gr.pos[0] - gr.radius < 0:
            gr.pos[0] = gr.radius
            if gr.vel[0] < 0.:
                gr.vel[0] *= -.9

        elif gr.pos[0] + gr.radius > 500:
            gr.pos[0] = 500 - gr.radius
            if gr.vel[0] > 0.:
                gr.vel[0] *= -.9

        if gr.pos[1] - gr.radius < 0:
            gr.pos[1] = gr.radius
            if gr.vel[1] < 0.:
                gr.vel[1] *= -.9
            
        elif gr.pos[1] + gr.radius > 100:
            gr.pos[1] = 100 - gr.radius
            if gr.vel[1] > 0.:
                gr.vel[1] *= -.9

def time_loop():
    apply_gravity()
    manage_contact()
    velocity_verlet()
    apply_boundaries()

if __name__ == "__main__":                
    # build domain
    pos = (10, 80)
    rad = 9
    grain  = dem.grain(pos, rad, 1.)
    grain.vel[0]=10
    dem.simu.xlim = (0,500)
    
    # and run simulation using the time_loop function
    dem.run(tot_iter_number=12000, update_plot_each=40, loop_fn=time_loop)
    print ("The end")







