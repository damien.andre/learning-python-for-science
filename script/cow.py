#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre, Alexandre Boulle


import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import numpy as np
import code # for debugging : use 'code.interact(local=locals())' somewhere to get a prompt


## read image 
img = mpimg.imread('./cow.png')
plt.figure()
plt.imshow(img)


print("image shape     :", img.shape)
print("image min value :", img.min())
print("image max value :", img.max())

## function for gray convertingg 
def rgb2gray(rgb_im):
    r, g, b = rgb_im[:,:,0], rgb_im[:,:,1], rgb_im[:,:,2]
    gray_im = 0.2989 * r + 0.5870 * g + 0.1140 * b
    return gray_im


## show image in gray scale
bw = rgb2gray(img)
plt.figure()
imgplot = plt.imshow(bw, cmap=plt.get_cmap('gray'))


## zoom to a given part
zoom = bw[200:500, 200:500]
plt.figure()
plt.imshow(zoom, cmap=plt.get_cmap('gray'))


## tresholding
zoom[zoom < 0.5] = 0
zoom[zoom > 0.5] = 1.
plt.figure()
plt.imshow(zoom, cmap=plt.get_cmap('gray'))


## rotate
plt.figure()
plt.imshow(zoom.T, cmap=plt.get_cmap('gray'))


## crop the figure with a black canva 
crop = np.zeros(bw.shape)
crop[200:500, 200:500] = zoom
plt.figure()
plt.imshow(crop, cmap=plt.get_cmap('gray'))


## hide a part of the image
zoom.fill(0)
plt.figure()
plt.imshow(bw, cmap=plt.get_cmap('gray'))


plt.show()
