#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre


it     = []
force  = []

with open('data.txt', 'r') as f:
    for line in f:
        if not '#' in line:
           data = line.split()
           it.append(int(data[0]))
           force.append(float(data[2]))

print(it)
print(force)

