#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2021, Damien Andre


import minidem as dem

# integration of motion with the velocity verlet algorithm 
def velocity_verlet():
    dt = dem.simu.dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

# the full time loop
def time_loop():
    velocity_verlet()
    print("elapsed time is", dem.simu.t, "s")
    dem.simu.print("elapsed time is", dem.simu.t, "s")

# the entry point of the program
if __name__ == "__main__":                
    # build domain with 4 grains at the corners
    rad        = 2
    density    = 1
    grain_1 = dem.grain(dem.vec(0  ,0  ), rad, density) 
    grain_2 = dem.grain(dem.vec(0  ,100), rad, density) 
    grain_3 = dem.grain(dem.vec(100,100), rad, density) 
    grain_4 = dem.grain(dem.vec(100,0  ), rad, density) 
    # now, we set an initial velocity to this grain
    grain_1.vel = dem.vec( 10, 10)
    grain_2.vel = dem.vec( 10,-10)
    grain_3.vel = dem.vec(-10,-10)
    grain_4.vel = dem.vec(-10, 10)
    # set the time step 
    dem.simu.dt = 0.004
    # and run simulation using the time_loop function
    dem.run(tot_iter_number=2000, update_plot_each=10, loop_fn=time_loop)
    print ("End of simulation, the elapsed time is", dem.simu.t, "s")







