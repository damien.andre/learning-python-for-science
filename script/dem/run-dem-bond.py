#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre


import minidem as dem
import random


dt = 0.002
t  = 0

# some list to identify left and right grains
# this list will be used to apply specific
# boundary conditions on these grains
left_grain  = []
right_grain = []


def reset_force():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., 0.)

def apply_gravity():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., -9.81*gr.mass)
        
def manage_contact():
    l = dem.lcm.compute_colliding_pair()
    for (gr1,gr2) in l:
        dem.contact(gr1, gr2, exclude_bonded_grain=True)

def manage_bond():
    for bond in dem.simu.bond_list:
        bond.update()
        
def velocity_verlet():
    global t, dt
    t = t + dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

def apply_boundaries():
    #here, we impose a constant velocicty on the 'left' and right 'grains'
    velocity = 1.
    for gr in left_grain:
        gr.pos[0] = gr.initial_pos[0] - t*velocity
        gr.pos[1] = gr.initial_pos[1]
        gr.vel[0] = -velocity 
        gr.vel[1] =  0. 
    for gr in right_grain:
        gr.pos[0] = gr.initial_pos[0] + t*velocity
        gr.pos[1] = gr.initial_pos[1]
        gr.vel[0] = velocity 
        gr.vel[1] = 0. 
    
def time_loop():
    reset_force()
    manage_bond()
    manage_contact()
    velocity_verlet()
    apply_boundaries()
        
if __name__ == "__main__":

    # load a compact domain
    dem.load_domain("compact-domain.txt")

    # build bonds for neighbour grains using contact detection with a margin coef
    margin_coef = 1.5
    l = dem.lcm.compute_colliding_pair(margin_coef)
    for (gr1,gr2) in l:
        if dem.in_contact(gr1,gr2, margin_coef):
            b = dem.bond(gr1, gr2)

    # fill the 'left' and 'righ' lists         
    for gr in dem.simu.grain_list.copy():
        if gr.pos[0] - gr.radius*1.1 < 0:
            gr.color = "orange"
            left_grain.append(gr)
        if gr.pos[0] + gr.radius*1.1 > 100:
            gr.color = "orange"
            right_grain.append(gr)
        if gr.pos[1]  + gr.radius*1.1 > 55:
            gr.remove()

    # change the dimension of the matplotlib graph
    dem.simu.xlim = (-30, 130)
    dem.simu.ylim = (-10 , 60)

    # run the simulation 
    dem.run(tot_iter_number=5000, update_plot_each=100, loop_fn=time_loop)
    print ("The end")


