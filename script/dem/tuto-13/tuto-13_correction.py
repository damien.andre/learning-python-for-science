#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2021, Damien Andre


import minidem as dem
import matplotlib.patches as patches

# useful variables 
label     = None
rectangle = None
count     = 0

# the rigid wall function 
def rigid_wall():
    global count
    f = 1. # the elastic/inelastic factor. It must be in the [0;1] range.
    for gr in dem.simu.grain_list:
        
        if gr.pos[0] - gr.radius < 0:
            gr.pos[0] = gr.radius
            if gr.vel[0] < 0.:
                gr.vel[0] *= -f
                gr.visible = not gr.visible
                count +=1

        elif gr.pos[0] + gr.radius > 120:
            gr.pos[0] = 120 - gr.radius
            if gr.vel[0] > 0.:
                gr.vel[0] *= -f
                gr.visible = not gr.visible
                count +=1
                
        if gr.pos[1] - gr.radius < 0:
            gr.pos[1] = gr.radius
            if gr.vel[1] < 0.:
                gr.vel[1] *= -f
                gr.visible = not gr.visible
                count +=1
                
        elif gr.pos[1] + gr.radius > 40:
            gr.pos[1] = 40 - gr.radius
            if gr.vel[1] > 0.:
                gr.vel[1] *= -f
                gr.visible = not gr.visible
                count +=1


# here, we add a function for resetting force 
def reset_force():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., 0.)
    

# integration of motion with the velocity verlet algorithm 
def velocity_verlet():
    dt = dem.simu.dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

# here we update the object such as the text label
def update_object():
    for gr in dem.simu.grain_list:
        label.set_position((gr.pos[0], gr.pos[1]-2*gr.radius))
        label.set_text(str(count))

    rectangle.set_height(2*(count+1))
        
# the full time loop
def time_loop():
    reset_force()
    velocity_verlet()
    rigid_wall()
    update_object()
    

# the entry point of the program
if __name__ == "__main__":                
    # build domain with one grain at (80,20)
    rad        = 2
    density    = 1
    # first layer of red balls
    grain_1 = dem.grain(dem.vec(80, 20), rad, density)
    # set the initial velocity 
    grain_1.vel = dem.vec(10, -5)
    # set domain limit
    dem.simu.xlim = (0,120)
    dem.simu.ylim = (0,40)
    # set the time step 
    dem.simu.dt = 0.004
    # add a text label
    label = dem.simu.ax.text(80, 20-2*rad, "0", transform=dem.simu.ax.transData, ha="center")
    dem.simu.add_object_to_scene(label)
    # add a rectange patch
    rectangle = patches.Rectangle((1 , 5), 10, 2, color="darkgray", alpha=.5)
    dem.simu.add_object_to_scene(rectangle)
    # disable title
    dem.simu.custom_title = True
    # disable numbers on axes
    dem.simu.ax.set_xticklabels([])
    dem.simu.ax.set_yticklabels([])
    # and run simulation using the time_loop function
    dem.run(tot_iter_number=10000, update_plot_each=10, loop_fn=time_loop)
    print ("End of simulation, the elapsed time is", dem.simu.t, "s")







