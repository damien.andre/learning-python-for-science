#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2021, Damien Andre


import minidem as dem


# the rigid wall function that uses simple reflection method.
# If a discrete element collide a wall, the velocity of the
# element is modified to simulate an elastic/inelastic collision.
def rigid_wall():
    f = 1. # the elastic/inelastic factor. It must be in the [0;1] range.
    for gr in dem.simu.grain_list:
        
        if gr.pos[0] - gr.radius < 0: # does the grain is in contact with the wall ?
            gr.pos[0] = gr.radius     # if yes, the position of the grain is corrected
            if gr.vel[0] < 0.:        # does the grain velocity tends to interpenetrate the wall ?
                gr.vel[0] *= -f       # if yes, the sign of the velocity is changed

        elif gr.pos[0] + gr.radius > 100:
            gr.pos[0] = 100 - gr.radius
            if gr.vel[0] > 0.:
                gr.vel[0] *= -f

        if gr.pos[1] - gr.radius < 0:
            gr.pos[1] = gr.radius
            if gr.vel[1] < 0.:
                gr.vel[1] *= -f
            
        elif gr.pos[1] + gr.radius > 100:
            gr.pos[1] = 100 - gr.radius
            if gr.vel[1] > 0.:
                gr.vel[1] *= -f



# here, we add a function for resetting force 
def reset_force():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., 0.)
    
        
# here, we add the contact management from the minidem module
def manage_contact():
    l = dem.lcm.compute_colliding_pair()
    for (gr1,gr2) in l:
        dem.contact(gr1,gr2)


# integration of motion with the velocity verlet algorithm 
def velocity_verlet():
    dt = dem.simu.dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

# the full time loop
def time_loop():
    reset_force()
    manage_contact()
    velocity_verlet()
    rigid_wall()
    

# the entry point of the program
if __name__ == "__main__":                
    # build domain with 4 grains at the corners
    rad        = 2
    density    = 1
    grain_1 = dem.grain(dem.vec(0  ,0  ), rad, density) 
    grain_2 = dem.grain(dem.vec(0  ,100), rad, density) 
    grain_3 = dem.grain(dem.vec(100,100), rad, density) 
    grain_4 = dem.grain(dem.vec(100,0  ), rad, density) 
    # new we set an initial velocity to this grain
    grain_1.vel = dem.vec( 40, 40)
    grain_2.vel = dem.vec( 40,-40)
    grain_3.vel = dem.vec(-40,-40)
    grain_4.vel = dem.vec(-40, 40)
    # edit color
    grain_1.color = "red"
    # set the time step 
    dem.simu.dt = 0.004
    # and run simulation using the time_loop function
    dem.run(tot_iter_number=4000, update_plot_each=10, loop_fn=time_loop)
    print ("End of simulation, the elapsed time is", dem.simu.t, "s")







