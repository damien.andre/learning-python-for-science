#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2021, Damien Andre


import minidem as dem
import random


# the driven grain position 
current_mouse_pos_x = 0
current_mouse_pos_y = 0

# start / stop
start = False

# the yellow driven grain
yellow_grain = None

def apply_boundary_condition():
    yellow_grain.pos[0] = current_mouse_pos_x
    yellow_grain.pos[1] = current_mouse_pos_y
    yellow_grain.vel[0] = 0.
    yellow_grain.vel[1] = 0.
        
def add_gravity_force():
    for gr in dem.simu.grain_list:
        gr.force += gr.mass*dem.vec(0., -9.81)

# the rigid wall function that uses simple reflection method.
def rigid_wall():
    f = 1. # the elastic/inelastic factor. It must be in the [0;1] range.
    for gr in dem.simu.grain_list:
        
        if gr.pos[0] - gr.radius < 0:
            gr.pos[0] = gr.radius
            if gr.vel[0] < 0.:
                gr.vel[0] *= -f

        elif gr.pos[0] + gr.radius > 100:
            gr.pos[0] = 100 - gr.radius
            if gr.vel[0] > 0.:
                gr.vel[0] *= -f

        if gr.pos[1] - gr.radius < 0:
            gr.pos[1] = gr.radius
            if gr.vel[1] < 0.:
                gr.vel[1] *= -f
            
        elif gr.pos[1] + gr.radius > 100:
            gr.pos[1] = 100 - gr.radius
            if gr.vel[1] > 0.:
                gr.vel[1] *= -f



# here, we add a function for resetting force 
def reset_force():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., 0.)
    
        
# here, we add the contact management from the minidem module
def manage_contact():
    l = dem.lcm.compute_colliding_pair()
    for (gr1,gr2) in l:
        dem.contact(gr1,gr2)


# integration of motion with the velocity verlet algorithm 
def velocity_verlet():
    dt = dem.simu.dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

# here, this is trick to get back the position of the mouse
def mouse_move(event):
    global current_mouse_pos_x, current_mouse_pos_y
    x, y = event.xdata, event.ydata
    if x != None :
        current_mouse_pos_x = x
    if y != None :
        current_mouse_pos_y = y

def on_press(event):
    global start
    if event.key == ' ': # if the key is space, toggle the start variable
        start = not start
        
# the full time loop
def time_loop():
    if (start) : 
        reset_force()
        add_gravity_force()
        manage_contact()
        velocity_verlet()
        rigid_wall()
        apply_boundary_condition()
        dem.simu.print("yellow grain position=", "(", int(current_mouse_pos_x), ",", int(current_mouse_pos_y), ")")


# the entry point of the program
if __name__ == "__main__":                
    # build domain with 4 grains at the corners
    rad        = 5
    density    = 1

    yellow_grain = dem.grain(dem.vec(current_mouse_pos_x, current_mouse_pos_y), rad, density)
    yellow_grain.color="yellow"
    
    for x in range(2*rad, 100-rad, 2*rad):
        for y in range(5*rad, 100-rad, 2*rad):
            gr = None
            if gr is None:
                x_rand = x + random.random() - 0.5
                y_rand = y + random.random() - 0.5
                rad_rand = rad - random.random()
                gr = dem.grain(dem.vec(x_rand, y_rand), rad_rand, density)
                gr.vel = dem.vec((random.random()-0.5)*10., (random.random()-0.5)*10.)

    # set the time step 
    dem.simu.dt = 0.005
    # we will use a custom title to display the number of loosing grain 
    dem.simu.custom_title = True
    # here this the trick to get the coordinate of the mouse 
    dem.simu.fig.canvas.mpl_connect('motion_notify_event', mouse_move)
    # here this the trick to catch 
    dem.simu.fig.canvas.mpl_connect('key_press_event', on_press)
    # print message
    dem.simu.print("press space to start/pause")
    # and run simulation using the time_loop function
    dem.run(tot_iter_number=1000000, update_plot_each=10, loop_fn=time_loop)
    print ("End of simulation, the elapsed time is", dem.simu.t, "s")







