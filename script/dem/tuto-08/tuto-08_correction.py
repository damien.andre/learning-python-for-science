#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2021, Damien Andre


import minidem as dem
import random 


# initialize an empty list. This list will contain all gray grains
gray_grain_list = [] 

def apply_boundary_condition():
    v = 1 # the velocity 
    for gr in gray_grain_list :
        x = gr.initial_pos[0] 
        y = gr.initial_pos[1] + v*dem.simu.t
        gr.pos = dem.vec(x,y)
        gr.vel = dem.vec(0,v)


def add_gravity_force():
    for gr in dem.simu.grain_list:
        gr.force += gr.mass*dem.vec(0., -9.81)

# the rigid wall function that uses simple reflection method.
# If a discrete element collide a wall, the velocity of the
# element is modified to simulate an elastic/inelastic collision.
def rigid_wall():
    f = 1. # the elastic/inelastic factor. It must be in the [0;1] range.
    for gr in dem.simu.grain_list:
        
        if gr.pos[0] - gr.radius < 0:
            gr.pos[0] = gr.radius
            if gr.vel[0] < 0.:
                gr.vel[0] *= -f

        elif gr.pos[0] + gr.radius > 100:
            gr.pos[0] = 100 - gr.radius
            if gr.vel[0] > 0.:
                gr.vel[0] *= -f

        if gr.pos[1] - gr.radius < 0:
            gr.pos[1] = gr.radius
            if gr.vel[1] < 0.:
                gr.vel[1] *= -f
            
        elif gr.pos[1] + gr.radius > 100:
            gr.pos[1] = 100 - gr.radius
            if gr.vel[1] > 0.:
                gr.vel[1] *= -f



# here, we add a function for resetting force 
def reset_force():
    for gr in dem.simu.grain_list:
        gr.force = dem.vec(0., 0.)
    
        
# here, we add the contact management from the minidem module
def manage_contact():
    l = dem.lcm.compute_colliding_pair()
    for (gr1,gr2) in l:
        dem.contact(gr1,gr2)


# integration of motion with the velocity verlet algorithm 
def velocity_verlet():
    dt = dem.simu.dt
    for gr in dem.simu.grain_list:
        a = gr.force/gr.mass
        gr.vel += (gr.acc + a) * (dt/2.)
        gr.pos += gr.vel * dt + 0.5*a*(dt**2.)
        gr.acc  = a

# the full time loop
def time_loop():
    reset_force()
    add_gravity_force()
    manage_contact()
    velocity_verlet()
    rigid_wall()
    apply_boundary_condition()
    

# the entry point of the program
if __name__ == "__main__":                
    # build domain with 4 grains at the corners
    rad        = 5
    density    = 1

    for x in range(2*rad, 100-rad, 2*rad):
        for y in range(2*rad, 100-rad, 2*rad):
            if y == 2*rad: # first layer
                gr = dem.grain(dem.vec(x, y), rad, density)
                gr.color = "gray"
                gray_grain_list.append(gr)
            else:
                x += random.random() - 0.5
                y += random.random() - 0.5
                rad_random = rad - random.random()
                gr = dem.grain(dem.vec(x, y), rad_random, density)
                gr.vel = dem.vec((random.random()-0.5)*10., (random.random()-0.5)*10.)

    # set the time step 
    dem.simu.dt = 0.004
    # and run simulation using the time_loop function
    dem.run(tot_iter_number=10000, update_plot_each=10, loop_fn=time_loop)
    print ("End of simulation, the elapsed time is", dem.simu.t, "s")







