#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Damien Andre

import numpy as np
import matplotlib.pyplot as plt
import scipy.io.wavfile
import sys

import matplotlib
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 30}
matplotlib.rc('font', **font)

wav_file   = 'sample1.wav'
rate, data = scipy.io.wavfile.read(wav_file)
time_step  = 1./rate

# manage mono or stereo signal
if data.ndim is 1:
    print("Analyzing mono wave file")
    signal = data
elif data.ndim is 2:
    print("Analysing stereo wave file")
    signal = np.array([x[0] for x in data])
else:
    print("unknow wave format, exiting...")
    sys.exit(0)

# temporal plot
time = np.arange(0, signal.size*time_step, time_step)
plt.plot(time, signal)
plt.xlabel('time (s)')
plt.ylabel('signal')
plt.title(wav_file)
plt.show()

# spectral analysis : computing the power spectral density 
yf  = np.fft.fft(signal)
N   = len(signal)
T   = 1./rate                   # the period of data
frq = np.fft.fftfreq(N, d=T)  # and the related frequencies

# plot spectral signal 
plt.plot(frq, np.abs(yf))
plt.xlim(0,5000)
plt.xlabel('freq (Hz)')
plt.ylabel('discrete fourier transform')
plt.title(wav_file)
plt.show()

psd  = (T/N) * np.abs(yf)**2  # compute power spectral density 
frq = frq[:int(N/2)] # remove negative frequencies
psd = psd[:int(N/2)] # and the related values of psd


# plot spectral signal 
plt.plot(frq, psd)
plt.xlim(0,5000)
plt.xlabel('freq (Hz)')
plt.ylabel('PSD')
plt.title(wav_file)
plt.show()


# compute notes, these values were extracted from https://fr.wikipedia.org/wiki/Note_de_musique
note_f = np.array([32.70, 65.41,  130.81, 261.63, 523.25, 1046.50, 2093.00, 4186.01,
                   34.65, 69.30,  138.59, 277.18, 554.37, 1108.73, 2217.46, 4434.92,
                   36.71, 73.42,  146.83, 293.66, 587.33, 1174.66, 2349.32, 4698.64,
                   38.89, 77.78,  155.56, 311.13, 622.25, 1244.51, 2489.02, 4978.03,
                   41.20, 82.41,  164.81, 329.63, 659.26, 1318.51, 2637.02, 5274.04,
                   43.65, 87.31,  174.61, 349.23, 698.46, 1396.91, 2793.83, 5587.65,
                   46.25, 92.50,  185.00, 369.99, 739.99, 1479.98, 2959.96, 5919.91,
                   49.00, 98.00,  196.00, 392.00, 783.99, 1567.98, 3135.96, 6271.93,
                   51.91, 103.83, 207.65, 415.30, 830.61, 1661.22, 3322.44, 6644.88,
                   55.00, 110.00, 220.00, 440.00, 880.00, 1760.00, 3520.00, 7040.00,
                   58.27, 116.54, 233.08, 466.16, 932.33, 1864.66, 3729.31, 7458.62,
                   61.74, 123.47, 246.94, 493.88, 987.77, 1975.53, 3951.07, 7902.00])

note_id = ['C ', 'C ', 'C ', 'C ', 'C ', 'C ', 'C ', 'C ',
           'C#', 'C#', 'C#', 'C#', 'C#', 'C#', 'C#', 'C#',
           'D ', 'D ', 'D ', 'D ', 'D ', 'D ', 'D ', 'D ', 
           'D#', 'D#', 'D#', 'D#', 'D#', 'D#', 'D#', 'D#',  
           'E ', 'E ', 'E ', 'E ', 'E ', 'E ', 'E ', 'E ', 
           'F ', 'F ', 'F ', 'F ', 'F ', 'F ', 'F ', 'F ', 
           'F#', 'F#', 'F#', 'F#', 'F#', 'F#', 'F#', 'F#', 
           'G ', 'G ', 'G ', 'G ', 'G ', 'G ', 'G ', 'G ', 
           'G#', 'G#', 'G#', 'G#', 'G#', 'G#', 'G#', 'G#', 
           'A ', 'A ', 'A ', 'A ', 'A ', 'A ', 'A ', 'A ', 
           'A#', 'A#', 'A#', 'A#', 'A#', 'A#', 'A#', 'A#', 
           'B ', 'B ', 'B ', 'B ', 'B ', 'B ', 'B ', 'B ']

# get the frequence of the max psd values 
rank = psd.argmax()
freq = frq[rank]
print("The main frequency of the wave file is {:.2f} Hz".format(freq))

# now we want the closest value of 'freq' in the Note list
idx = (np.abs(note_f-freq)).argmin()
print("Analyzing '{}'... Closest note is '{}' with a difference of {:.2f} Hz".format(wav_file, note_id[idx], np.abs(freq-note_f[idx])))
