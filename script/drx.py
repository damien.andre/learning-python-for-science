#!/usr/bin/python
# -*- coding: utf-8 -*-
# MIT License, Copyright (c) 2018, Alexandre Boulle, Damien Andre

import numpy as np
import matplotlib.pyplot as plt
import glob
from scipy.optimize import curve_fit

# the gaussian function to fit 
def gauss(x,mu,sigma,A,K):
    A1 = A/(sigma*(2.*np.pi)**0.5)
    return K+A1*np.exp(-.5*np.power((x-mu)/sigma,2))

# function for reading *.xy drx file
# it returns both angle and intensity as numpy array
def read_xy(fname):
    # initilize empty list, we will fill these lists with data from files
    angle     = []
    intensity = []

    # open a file 
    with open(fname) as f:
        # read the file line by line
        for line in f:
            # data are separated by comma, let's split it
            res = line.split(",")
            # append data, to list, note that these data are string
            angle.append(res[0])
            intensity.append(res[1])
    # convert the list to numpy.array and forcing the type to be floating point numbers
    angle     = np.array(angle    , dtype=float)
    intensity = np.array(intensity,dtype=float)
    return angle, intensity


# we focus on the following boundaries that gives a single drx peak
lo, hi = 23, 24.5

# let's find all the files that end by '.xy'  
xy_file = glob.glob("*.xy")

# we iterate on these files
for i, fname in enumerate(xy_file):

    # we extract data : 'x' is the angle and 'y' the intensity
    x, y = read_xy(fname)
    
    # we focus in the [lo, li] range
    x_zoom, y_zoom = x[x>lo],y[x>lo]
    x_zoom, y_zoom = x_zoom[x_zoom<hi], y_zoom[x_zoom<hi]
    
    # we get the postion angle of the peak
    pos =  x_zoom[y_zoom==y_zoom.max()][0]

    # we compute the area of the peak 
    area = y_zoom.sum()*(x_zoom[1]-x_zoom[0])

    # we compute the background noise
    bkg = y_zoom.min()

    # all these three values 'pos', 'area' and bkg are used as
    # initial guess for the fitting algorithm
    p_guess = [pos, 0.05, area, bkg]

    # let's use curve_fit from the scipy.optimize module for
    # retrieving parameters of the gaussian function
    popt, pcov = curve_fit(gauss, x_zoom, y_zoom,p_guess)

    # now, we use these parameters for computing the fitting gaussian
    fit = gauss(x_zoom, *popt)

    # let's plot
    plt.subplot(len(xy_file),1,i+1)
    plt.plot(x_zoom, y_zoom, label = fname)          # plot experimental data
    plt.plot(x_zoom, fit)                            # plot fitting function
    plt.axvline(popt[0])                             # draw vertical line at peak
    plt.text(pos+0.02, 0, "{:.4f}°".format(popt[0])) # display peak position
    plt.xlabel("angle (degree)")
    plt.ylabel("intensity")
    plt.legend()

plt.show()

